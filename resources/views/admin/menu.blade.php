<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ url('templates/admin') }}/dist/img/37820087_2153520808225890_7196554730531717120_n.jpg " class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ \Auth::user()->name }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
    <?php
    $allow_route = [];

    $arr_permission = get_allow_route_name(\Auth::user()->id_group);
    // dd($arr_permission);
    if (isset($arr_permission)) {
        $allow_route = json_decode($arr_permission->permission, true);
    }


    // var_dump($allow_route);
    ?>

    <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">

            @php
                $array_menu = [];
                $argv_0['header'] = 'ĐƠN HÀNG';
                $argv_0['menu'] = [
                    
                    [
                        'name' => 'Danh sách đặt hàng' ,
                        'route' => 'admin.loai-phim.list',
                        'alias' => 'ql_datve',
                    ]
                ];
                $array_menu[]= $argv_0;
                $argv_1['header'] = 'Quản lý ';
                $argv_1['menu'] = [
                    [
                        'name3' => 'QL loại phim',
                        'alias' => 'ql_phim1',
                        'route' => '',
                        'submenu'=>[
                            ['name'=>'Danh sách loại Phim','route' => 'admin.loai-phim.list'],
                            ['name'=>'Thêm loại phim ','route' => 'admin.loai-phim.create']
                        ]
                    ]



                ];
                $array_menu[]= $argv_1;

                $argv_2['header'] = 'Cài đặt ';
                $argv_2['menu'] = [
                     [
                        'name' => 'User',
                        'alias' => 'user',
                        'route' => '',
                        'submenu'=>[
                            ['name'=>'List User','route' => 'admin.user.list'],
                            ['name'=>'Add User','route' => 'admin.user.add'],
                        ]
                    ],
                    [
                        'name' => 'Group user',
                        'route' => 'admin.group.list',
                        'alias' => 'group_user',
                    ],
                ];
                $array_menu[]= $argv_2;

            @endphp

            @foreach($array_menu as $item)
                <li class="header">{{ $item['header'] }}</li>
                @foreach($item['menu'] as $key => $menu)
                    <?php
                    $hide = '';
                    if (!isset($menu['submenu'])) {
                        $a = [];
                        $a = $menu['route']; // var_dump(in_array($a, $allow_route));
                        $hide = !empty($a) && !empty($allow_route) && in_array($a, $allow_route) ? '' : 'display: none';
                    } else {
                        $hide = '';
                    }
                    ?>
                    <li style="{{\Auth::user()->id_group!=0 ? $hide : ''}}"
                        class="@if(isset($menu['submenu'])) treeview @endif  {{ isset($menu['submenu']) && in_array(get_current_route_name(),array_pluck($menu['submenu'],'route')) == true  ? 'menu-open'  : ''}}">
                        <a href="{{ $menu['route'] != '' ? route($menu['route']) : '#' }}">
                            @if(isset($menu['name']))
                            <i class="fa fa-dashboard"></i> <span>{{ $menu['name'] }}</span>
                            @elseif(isset($menu['name1']))
                                <i class="fa fa-building-o"></i> <span>{{ $menu['name1'] }}</span>
                            @elseif(isset($menu['name2']))
                                <i class="fa fa-money"></i> <span>{{ $menu['name2'] }}</span>
                            @elseif(isset($menu['name3']))
                                <i class="glyphicon glyphicon-picture"></i> <span>{{ $menu['name3'] }}</span>
                            @elseif(isset($menu['name4']))
                                <i class="fa fa-unsorted"></i> <span>{{ $menu['name4'] }}</span>
                            @elseif(isset($menu['name5']))
                                <i class="fa fa-user-md"></i> <span>{{ $menu['name5'] }}</span>
                            @elseif(isset($menu['name6']))
                                <i class="fa fa-calendar-check-o"></i> <span>{{ $menu['name6'] }}</span>
                            @elseif(isset($menu['name7']))
                                <i class="fa fa-building-o"></i> <span>{{ $menu['name7'] }}</span>
                            @elseif(isset($menu['name8']))
                                <i class="fa fa-user"></i> <span>{{ $menu['name8'] }}</span>
                             @elseif(isset($menu['name9']))
                                <i class="fa fa-building-o"></i> <span>{{ $menu['name9'] }}</span>    
                            @endif
                            @if(isset($menu['submenu']))
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            @endif
                        </a>
                        @if(isset($menu['submenu']))
                            <ul class="treeview-menu    "
                                style="{{ in_array(get_current_route_name(),array_pluck($menu['submenu'],'route')) == true  ? 'display: block;'  : ''}} ">
                                <?php
                                $num_sub = count($menu['submenu']);
                                ?>
                                @foreach($menu['submenu'] as $sub)
                                    <?php
                                    $i = 0;
                                    $hide_sub = '';
                                    $b = [];
                                    $b = $sub['route'];
                                    // if(!in_array($b, $allow_route)){
                                    //     $hide_sub = 'display: none';
                                    //     $i++;
                                    // }
                                    // else {
                                    //     $hide_sub = '';
                                    // }
                                    // var_dump($b);
                                    ?>
                                    @if((!empty($b) && !empty($allow_route) && in_array($b, $allow_route)) || \Auth::user()->id_group==0)
                                        <li class="active">
                                            <a href="{{ $sub['route'] != '' ? route($sub['route']) : '#' }}">
                                                <i class="fa fa-circle-o"></i>{{ $sub['name'] }}</a>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        @endif
                    </li>
                @endforeach

            @endforeach


        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
<script>
    $(function () {
        $('.sidebar-menu li.treeview>ul.treeview-menu').each(function () {
            var hasChild = $(this).find('>li').length > 0;
            if (!hasChild) {
                $(this).parent().remove();
            }
        });
    });
</script>
