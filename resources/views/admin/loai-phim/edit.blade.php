@extends('admin.master') @section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Sửa loại phim
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="#">
                    <i class="fa fa-dashboard"></i> Home</a>
            </li>
            <li class="active">Sửa loại phim</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-12">
                                    <form action="{{ route('admin.loai-phim.update',['id'=>$data->id]) }}" method="post" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="col-sm-6">
                                            @if(count($errors)>0)
                                                <div class="alert alert-danger">
                                                    @foreach($errors->all() as $er)
                                                        {{$er}}<br>
                                                    @endforeach    
                                                </div>
                                            @endif    
                                            @if(session('thongbao'))
                                                <div class="alert alert-danger">
                                                    {{session('thongbao')}}
                                                </div>
                                            @endif
                                            <div class="form-group">
                                                <div class="row">
                                                    <label class="col-sm-5 control-label">Tên loại phim</label>
                                                    <div class="col-sm-7">
                                                        <input name="tenloaiphim" class="form-control" value="{{ isset($data) ? $data->tenloaiphim : '' }}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <div class="row">
                                                    <input value="Lưu" class="btn btn-info pull-right" type="submit">
                                                    <a onclick="return confirm('Quay lại danh sách loại phim?') ;" href="{{ route('admin.loai-phim.list') }}" class="btn btn-info pull-right"
                                                        style="margin-right: 15px">Quay lại
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection