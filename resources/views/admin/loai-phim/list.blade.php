@extends('admin.master')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Danh sách loại phim
                <small>
            <a class="btn btn-success" href="{{route('admin.loai-phim.create')}}"> Thêm mới </a>
             <a href="{{ route('admin.loai-phim.list') }}" class="btn btn-danger">Làm mới</a>
                </small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Danh sách loại sản phẩm</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content table-border">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-solid box-primary">
                        <div class="box-header">
                            <h3 class="box-title">Danh sách loại phim </h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body custom-table">
                            @if(count($errors)>0)
                                                <div class="alert alert-danger">
                                                    @foreach($errors->all() as $er)
                                                        {{$er}}<br>
                                                    @endforeach    
                                                </div>
                                            @endif    
                                            @if(session('thongbao'))
                                                <div class="alert alert-danger">
                                                    {{session('thongbao')}}
                                                </div>
                                            @endif
                            <div class="table-responsive">
                                <table id="example" class="table table-striped table-bordered" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>STT</th>
                                                    <th width="140px">Chức năng</th>
                                                    <th>Tên loại phim</th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($data as $item)
                                                <tr>
                                                    <td style="text-align: center;"></td>
                                                    <td style="text-align: center;">
                                                        <a href="{{ route('admin.loai-phim.edit', $item->id) }}" class="fa fa-edit" aria-hidden="true" style="border: 5px solid blue; border-radius: 4px; height: 50%; color: white; background-color: blue;"></a >
                                                        <a href="{{ route('admin.loai-phim.show', $item->id) }}" class="fa fa-eye" aria-hidden="true" style="border: 5px solid yellow; border-radius: 4px; height: 50%; color: white; background-color: yellow;"> 
                                                        </a>

                                                        <a onclick="return confirm('Bạn có muốn xóa không?') ;" href="{{ route('admin.loai-phim.destroy', $item->id) }}" class="fa fa-trash aria-hidden="true" style="border: 5px solid red; border-radius: 4px; height: 50%; color: white; background-color: red;"></a>
                                                    </td>
                                                    <td>{{$item->tenloaiphim}}</td>
                                                    
                                                </tr>
                                                 @endforeach
                                            </tbody>
                                </table>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('footer')
<script>
    $(document).ready(function() {
    var t = $('#example').DataTable( {
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        } ],
        "order": [[ 1, 'asc' ]]
    } );
 
    t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
} );
    function numberpage() {
            var x = document.getElementById("mySelect").value;
            document.getElementById("demo").innerHTML = x;
        }
    $(document).ready(function () {
        $.urlParam = function (name) {
            var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
            if (results == null) {
                return null;
            }

            else {
                return decodeURI(results[1]) || 0;
            }
        }
        $("form#importFormSubmit").submit(function (e) {
                                if(document.getElementById("file-import").files.length == 0)
                                {
                                    $('#importModals .modal-body .list-errors').html('Vui lòng chọn File');
                                }else{
                                    $('#importModals .modal-body .list-errors').html('Đang tải...');
                                }
                                e.preventDefault();
                                console.log('import ho khau')
                // $('#importModals .modal-body .list-errors').html('Đang tải....');
                var formData = new FormData(this);

                $.ajax({
                    url: '/admin/ajax/don-vi-tinh/import',
                    type: 'POST',
                    data: formData,
                    success: function (data) {
                        $('#importModals .modal-body .list-errors').html('');
                        if (data.code == 200) {
                            $('#importModals').modal('hide');
                            alert('Import success');
                        }
                        else {
                            var errors = data.data;
                            for (var i = 0; i < errors.length; i++) {
                                console.log(errors[i]);
                                $('#importModals .modal-body .list-errors').append('<p>' + errors[i] + '</p>'
                                    )
                                ;
                            }
                        }

                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
            });
    });    
</script>
@endsection