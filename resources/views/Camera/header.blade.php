<header class="fixed-top">
            <nav class="navbar navbar-expand-lg">

                <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon leftmenutrigger animate">
                        <span></span>
                        <span></span>
                        <span></span>
                    </span>
                </button>

                <a class="navbar-brand animate color-9395A9" href="{{ route('camera.home.list')}}">CY<span
                        class="color-393B60">WATCH</span></a>
                <div class="search"><a href="#"><i class="fas fa-search"></i></a></div>
                <div class="cart"><a href="cart.html"><i class="fas fa-shopping-cart"></i></a></div>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav">
                        <li class="nav-item active">
                            <a class="nav-link color-393B60" href="{{ route('camera.home.list')}}">Trang chủ<span
                                    class="sr-only">(current)</span></a>
                        </li>

                        <li class="nav-item dropdown">
                            <a class="nav-link color-9395A9" href="#" id="navbarDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Sản phẩm <i
                                    class="fas fa-angle-down"></i></a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">

                                <div class="dropdown-menu-wrap1">

                                    <div class="brand-list1">
                                        <p>Thương hiệu</p>
                                        <a class="d-block" href="{{ route('list_of_product.list')}}">Apple</a>
                                        <a class="d-block" href="{{ route('list_of_product.list')}}">Fitbit</a>
                                        <a class="d-block" href="{{ route('list_of_product.list')}}">Garmin</a>
                                        <a class="d-block" href="{{ route('list_of_product.list')}}">Fossil</a>
                                        <a class="d-block" href="{{ route('list_of_product.list')}}">Samsung</a>
                                        <a class="d-block" href="{{ route('list_of_product.list')}}">Huawei</a>
                                    </div>

                                    <div class="collection-list1">
                                        <p>Bộ sưu tập</p>
                                        <div class="collection-list-item">
                                            <a href="{{ route('list_of_product.list')}}">
                                                <div class="collection-list-item1">
                                                    <img src="{{asset('/templates/web/')}}/images/3r.jpg" alt="">
                                                    <p>Activity Trackers</p>
                                                </div>
                                            </a>

                                            <a href="{{ route('list_of_product.list')}}">
                                                <div class="collection-list-item2">
                                                    <img src="{{asset('/templates/web/')}}/images/3r1.jpg" alt="">
                                                    <p>Sport Watches</p>
                                                </div>
                                            </a>

                                            <a href="{{ route('list_of_product.list')}}">
                                                <div class="collection-list-item3">
                                                    <img src="{{asset('/templates/web/')}}/images/2r.jpg" alt="">
                                                    <p>Smart Watches</p>
                                                </div>
                                            </a>

                                        </div>
                                    </div>

                                </div>

                                <div class="dropdown-menu-wrap2">

                                    <div class="brand-list2">
                                        <p>Thương hiệu</p>
                                        <a class="d-block" href="{{ route('list_of_product.list')}}">Apple</a>
                                        <a class="d-block" href="{{ route('list_of_product.list')}}">Fitbit</a>
                                        <a class="d-block" href="{{ route('list_of_product.list')}}">Garmin</a>
                                        <a class="d-block" href="{{ route('list_of_product.list')}}">Fossil</a>
                                        <a class="d-block" href="{{ route('list_of_product.list')}}">Samsung</a>
                                        <a class="d-block" href="{{ route('list_of_product.list')}}">Huawei</a>
                                    </div>

                                    <div class="collection-list2">
                                        <p>Bộ sưu tập</p>
                                        <a class="d-block" href="{{ route('list_of_product.list')}}">Activity Trackers</a>
                                        <a class="d-block" href="{{ route('list_of_product.list')}}">Sport Watches</a>
                                        <a class="d-block" href="{{ route('list_of_product.list')}}">Smart Watches</a>
                                    </div>

                                </div>

                            </div>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link color-9395A9" href="{{ route('promotion.list')}}">Khuyến mại</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link color-9395A9" href="{{ route('contact.list')}}">Liên hệ</a>
                        </li>
                    </ul>
                </div>

            </nav>
        </header>