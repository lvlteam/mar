@extends('Camera.master')
@section('content')
@include('Camera.header')
<!-- ===== BANNER ===== -->
<aside>
        <img src="{{asset('/templates/web/')}}/images/Smartwatch-Collection.jpg" alt="Advertisement Image">
      </aside>
      <!-- ===== END OF BANNER ===== -->

      <section>

        <!-- ===== BREADCRUMB ===== -->
        <div class="breadcrumb-lop">
          <a href="index.html">Trang chủ</a> > Danh mục sản phẩm
        </div>
        <!-- ===== END OF BREADCRUMB ===== -->

        <div class="lop-wrap">

          <div class="lop-filter">

            <div class="lop-filter1">
            
                <div class="dropdown dropdown1">
                  <a class="btn btn1 dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                    Bộ sưu tập
                  </a>
            
                  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                    <input type="checkbox" name="collection-lop" value="at" checked> Activity Trackers<br>
                    <input type="checkbox" name="collection-lop" value="spw"> Sport Watches<br>
                    <input type="checkbox" name="collection-lop" value="smw"> Smart Watches<br>
                  </div>
                </div>
            
                <div class="dropdown dropdown2">
                  <a class="btn btn2 dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                    Thương hiệu
                  </a>
            
                  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                    <input type="checkbox" name="brand-lop" value="all" checked> Tất cả<br>
                    <input type="checkbox" name="brand-lop" value="apple"> Apple<br>
                    <input type="checkbox" name="brand-lop" value="fitbit"> Fitbit<br>
                    <input type="checkbox" name="brand-lop" value="fossil"> Fossil<br>
                    <input type="checkbox" name="brand-lop" value="samsung"> Samsung<br>
                    <input type="checkbox" name="brand-lop" value="huawei"> Huawei<br>
                  </div>
                </div>
            
                <div class="dropdown dropdown3">
                  <a class="btn dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                    Giá
                  </a>
            
                  <div class="dropdown-menu dropdown-menu-right dropdown-menu-sm-right" aria-labelledby="dropdownMenuLink">
                    <input type="radio" name="price" value="all" checked> Tất cả<br>
                    <input type="radio" name="price" value="prom"> Khuyến mại<br>
                    <input type="radio" name="price" value="p1"> 100.000<sup>đ</sup> - 500.000<sup>đ</sup><br>
                    <input type="radio" name="price" value="p2"> 500.000<sup>đ</sup> - 1.000.000<sup>đ</sup><br>
                    <input type="radio" name="price" value="p3"> 1.000.000<sup>đ</sup> - 2.000.000<sup>đ</sup><br>
                    <input type="radio" name="price" value="p4"> 2.000.000<sup>đ</sup> - 5.000.000<sup>đ</sup><br>
                    <input type="radio" name="price" value="p5"> Trên 5.000.000<sup>đ</sup><br>
                  </div>
                </div>
            
            </div>

            <div class="lop-filter2">
              <span>Sắp xếp theo</span>
              <div>
                <select class="form-control">
                  <option value="1">Giá thấp</option>
                  <option value="2">Giá cao</option>
                  <option value="3">Bán chạy</option>
                  <option value="4">Mới về</option>
                </select>
              </div>
            </div>

          </div>

            <!-- ===== MODAL THÊM VÀO GIỎ HÀNG ==== -->
            <div class="modal fade" id="modal-cart" tabindex="-1" role="dialog" aria-labelledby="modal-w1-i1-label" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
            
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    <h5 class="modal-title">THÊM VÀO GIỎ HÀNG</h5>
                  </div>
            
                  <div class="modal-body d-flex">
                    <div class="modal-img">
                      <img class="border" src="{{asset('/templates/web/')}}/images/3r.jpg" alt="" />
                    </div>
                    <div class="modal-info">
                      <h4><a href="product_detail.html">Fitbit Blaze Smart Fitness Watch</a></h4>
                      <div class="modal-price mt-2">Giá: <span>1.000.000<sup>đ</sup></span></div>
                      <div class="modal-qty mt-3">Số lượng: <span style="font-weight: bold;">1</span></div>
                    </div>
                  </div>
            
                  <div class="modal-footer">
                    <button type="button" class="btn btn-muted" data-dismiss="modal">Tiếp tục mua hàng</button>
                    <button type="button" class="btn btn-success" onclick="window.location.href = 'cart.html';">Xem giỏ hàng</button>
                  </div>
            
                </div>
              </div>
            </div>
            <!-- END OF MODAL THÊM VÀO GIỎ HÀNG -->

            <div class="lop-products">
              <div class="row">
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 wrap-item animate position-relative hidden-border animate">
                  <a href="product_detail.html">
                    <div class="wrap-img cursor-pointer position-absolute">
                      <img src="{{asset('/templates/web/')}}/images/3r.jpg" alt="First slide" />
                    </div>
                  </a>
                  <div class="promo-symbol position-absolute animate">
                    <p class="position-absolute text-light">SALE</p>
                  </div>
              
                  <i class="fas fa-cart-plus animate position-absolute cursor-pointer" data-toggle="modal"
                    data-target="#modal-cart"></i>
              
                  <div class="info position-absolute">
                    <p class="price">1.000.000<sup>đ</sup></p>
                    <p class="old-price">1.200.000<sup>đ</sup></p>
                  </div>
                  <div class="product-name position-absolute">Fitbit Blaze Smart Fitness</div>
                </div>
              
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 wrap-item animate position-relative hidden-border animate">
                  <a href="product_detail.html">
                    <div class="wrap-img cursor-pointer position-absolute">
                      <img src="{{asset('/templates/web/')}}/images/3r.jpg" alt="Second slide" />
                    </div>
                  </a>
                  <!-- <div class="promo-symbol position-absolute animate">
                                  <p class="position-absolute text-light">SALE</p>
                                </div> -->
              
                  <i class="fas fa-cart-plus animate position-absolute cursor-pointer" data-toggle="modal"
                    data-target="#modal-cart"></i>
              
                  <div class="info position-absolute">
                    <p class="price">2.100.000<sup>đ</sup></p>
                    <!-- <p class="old-price">2.500.000<sup>đ</sup></p> -->
                  </div>
                  <div class="product-name position-absolute">Garmin vívosmart HR</div>
                </div>
              
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 wrap-item animate position-relative hidden-border animate">
                  <a href="product_detail.html">
                    <div class="wrap-img cursor-pointer position-absolute">
                      <img src="{{asset('/templates/web/')}}/images/3r.jpg" alt="Second slide" />
                    </div>
                  </a>
                  <!-- <div class="promo-symbol position-absolute animate">
                                  <p class="position-absolute text-light">SALE</p>
                                </div> -->
              
                  <i class="fas fa-cart-plus animate position-absolute cursor-pointer" data-toggle="modal"
                    data-target="#modal-cart"></i>
              
                  <div class="info position-absolute">
                    <p class="price">5.000.000<sup>đ</sup></p>
                    <!-- <p class="old-price">6.000.000<sup>đ</sup></p> -->
                  </div>
                  <div class="product-name position-absolute">Fossil Q Gen 3 Smartwatch</div>
                </div>
              
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 wrap-item animate position-relative hidden-border animate">
                  <a href="product_detail.html">
                    <div class="wrap-img cursor-pointer position-absolute">
                      <img src="{{asset('/templates/web/')}}/images/3r.jpg" alt="Second slide" />
                    </div>
                  </a>
                  <div class="promo-symbol position-absolute animate">
                    <p class="position-absolute text-light">SALE</p>
                  </div>
              
                  <i class="fas fa-cart-plus animate position-absolute cursor-pointer" data-toggle="modal"
                    data-target="#modal-cart"></i>
              
                  <div class="info position-absolute">
                    <p class="price">1.900.000<sup>đ</sup></p>
                    <p class="old-price">2.100.000<sup>đ</sup></p>
                  </div>
                  <div class="product-name position-absolute">Samsung Gear Sport</div>
                </div>
              
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 wrap-item animate position-relative hidden-border animate">
                  <a href="product_detail.html">
                    <div class="wrap-img cursor-pointer position-absolute">
                      <img src="{{asset('/templates/web/')}}/images/3r.jpg" alt="Second slide" />
                    </div>
                  </a>
                  <!-- <div class="promo-symbol position-absolute animate">
                                  <p class="position-absolute text-light">SALE</p>
                                </div> -->
              
                  <i class="fas fa-cart-plus animate position-absolute cursor-pointer" data-toggle="modal"
                    data-target="#modal-cart"></i>
              
                  <div class="info position-absolute">
                    <p class="price">1.000.000<sup>đ</sup></p>
                    <!-- <p class="old-price">1.250.000<sup>đ</sup></p> -->
                  </div>
                  <div class="product-name position-absolute">Apple Watch Series 3</div>
                </div>
              
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 wrap-item animate position-relative hidden-border animate">
                  <a href="product_detail.html">
                    <div class="wrap-img cursor-pointer position-absolute">
                      <img src="{{asset('/templates/web/')}}/images/3r.jpg" alt="Second slide" />
                    </div>
                  </a>
                  <div class="promo-symbol position-absolute animate">
                    <p class="position-absolute text-light">SALE</p>
                  </div>
              
                  <i class="fas fa-cart-plus animate position-absolute cursor-pointer" data-toggle="modal"
                    data-target="#modal-cart"></i>
              
                  <div class="info position-absolute">
                    <p class="price">3.000.000<sup>đ</sup></p>
                    <p class="old-price">4.000.000<sup>đ</sup></p>
                  </div>
                  <div class="product-name position-absolute">Garmin Forerunner 235</div>
                </div>
              
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 wrap-item animate position-relative hidden-border animate">
                  <a href="product_detail.html">
                    <div class="wrap-img cursor-pointer position-absolute">
                      <img src="{{asset('/templates/web/')}}/images/3r.jpg" alt="First slide" />
                    </div>
                  </a>
                  <!-- <div class="promo-symbol position-absolute animate">
                                    <p class="position-absolute text-light">SALE</p>
                                  </div> -->
              
                  <i class="fas fa-cart-plus animate position-absolute cursor-pointer" data-toggle="modal"
                    data-target="#modal-cart"></i>
              
                  <div class="info position-absolute">
                    <p class="price">1.000.000<sup>đ</sup></p>
                    <!-- <p class="old-price">1.200.000<sup>đ</sup></p> -->
                  </div>
                  <div class="product-name position-absolute">Fitbit Blaze Smart Fitness</div>
                </div>
              
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 wrap-item animate position-relative hidden-border animate">
                  <a href="product_detail.html">
                    <div class="wrap-img cursor-pointer position-absolute">
                      <img src="{{asset('/templates/web/')}}/images/3r.jpg" alt="Second slide" />
                    </div>
                  </a>
                  <div class="promo-symbol position-absolute animate">
                    <p class="position-absolute text-light">SALE</p>
                  </div>
              
                  <i class="fas fa-cart-plus animate position-absolute cursor-pointer" data-toggle="modal"
                    data-target="#modal-cart"></i>
              
                  <div class="info position-absolute">
                    <p class="price">2.100.000<sup>đ</sup></p>
                    <p class="old-price">2.500.000<sup>đ</sup></p>
                  </div>
                  <div class="product-name position-absolute">Garmin vívosmart HR</div>
                </div>
              
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 wrap-item animate position-relative hidden-border animate">
                  <a href="product_detail.html">
                    <div class="wrap-img cursor-pointer position-absolute">
                      <img src="{{asset('/templates/web/')}}/images/3r.jpg" alt="Second slide" />
                    </div>
                  </a>
                  <div class="promo-symbol position-absolute animate">
                    <p class="position-absolute text-light">SALE</p>
                  </div>
              
                  <i class="fas fa-cart-plus animate position-absolute cursor-pointer" data-toggle="modal"
                    data-target="#modal-cart"></i>
              
                  <div class="info position-absolute">
                    <p class="price">5.000.000<sup>đ</sup></p>
                    <p class="old-price">6.000.000<sup>đ</sup></p>
                  </div>
                  <div class="product-name position-absolute">Fossil Q Gen 3 Smartwatch</div>
                </div>
              
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 wrap-item animate position-relative hidden-border animate">
                  <a href="product_detail.html">
                    <div class="wrap-img cursor-pointer position-absolute">
                      <img src="{{asset('/templates/web/')}}/images/3r.jpg" alt="Second slide" />
                    </div>
                  </a>
                  <!-- <div class="promo-symbol position-absolute animate">
                                  <p class="position-absolute text-light">SALE</p>
                                </div> -->
              
                  <i class="fas fa-cart-plus animate position-absolute cursor-pointer" data-toggle="modal"
                    data-target="#modal-cart"></i>
              
                  <div class="info position-absolute">
                    <p class="price">1.900.000<sup>đ</sup></p>
                    <!-- <p class="old-price">2.100.000<sup>đ</sup></p> -->
                  </div>
                  <div class="product-name position-absolute">Samsung Gear Sport</div>
                </div>
              
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 wrap-item animate position-relative hidden-border animate">
                  <a href="product_detail.html">
                    <div class="wrap-img cursor-pointer position-absolute">
                      <img src="{{asset('/templates/web/')}}/images/3r.jpg" alt="Second slide" />
                    </div>
                  </a>
                  <!-- <div class="promo-symbol position-absolute animate">
                                  <p class="position-absolute text-light">SALE</p>
                                </div> -->
              
                  <i class="fas fa-cart-plus animate position-absolute cursor-pointer" data-toggle="modal"
                    data-target="#modal-cart"></i>
              
                  <div class="info position-absolute">
                    <p class="price">1.000.000<sup>đ</sup></p>
                    <!-- <p class="old-price">1.250.000<sup>đ</sup></p> -->
                  </div>
                  <div class="product-name position-absolute">Apple Watch Series 3</div>
                </div>
              
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 wrap-item animate position-relative hidden-border animate">
                  <a href="product_detail.html">
                    <div class="wrap-img cursor-pointer position-absolute">
                      <img src="{{asset('/templates/web/')}}/images/3r.jpg" alt="Second slide" />
                    </div>
                  </a>
                  <!-- <div class="promo-symbol position-absolute animate">
                                  <p class="position-absolute text-light">SALE</p>
                                </div> -->
              
                  <i class="fas fa-cart-plus animate position-absolute cursor-pointer" data-toggle="modal"
                    data-target="#modal-cart"></i>
              
                  <div class="info position-absolute">
                    <p class="price">3.000.000<sup>đ</sup></p>
                    <!-- <p class="old-price">4.000.000<sup>đ</sup></p> -->
                  </div>
                  <div class="product-name position-absolute">Garmin Forerunner 235</div>
                </div>
              
              </div>
            </div>

            <div class="pagination">
              <div>
                <span class="arrow-lop"><a href="#"><</a></span>
                <span class="pagi-mark"><a href="#">1</a></span>
                <span><a href="#">2</a></span>
                <span>...</span>
                <span><a href="#">6</a></span>
                <span><a href="#">7</a></span>
                <span class="arrow-lop"><a href="#">></a></span>
              </div>
            </div>
                    
        </div>

        <!-- ===== ADVERTISEMENT ===== -->
        <div class="advert wow fadeIn position-relative">
          <a href="promotion.html">
            <div class="position-absolute ad-box">
              <p class="ad-txt1 position-absolute text-white">SPRING SALE</p>
              <P class="ad-txt2 position-absolute color-C3C3CE">GIẢM ĐẾN <span class="text-white">50%</span></P>
            </div>
          </a>
        </div>
        <!-- ===== END OF ADVERTISEMENT ===== -->
      </section>

<!-- ===== FOOTER ===== -->
@include('Camera.footer')

<div class="back2top hide-item position-fixed">
  <i class="fas fa-chevron-circle-up"></i>
</div>
@endsection