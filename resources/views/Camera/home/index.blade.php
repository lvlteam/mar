@extends('Camera.master')
@section('content')
@include('Camera.header')

        <!-- ===== BIG CAROUSEL PROMOTION ===== -->
        <aside class="promotion">

            <div class="promotion1 position-relative">
                <img src="{{asset('/templates/web/')}}/images/carousel1.jpg" alt="">
                <div class="promo1-box position-absolute">
                    <p class="promo-txt1 position-absolute">FITBIT BLAZE</p>
                    <p class="promo-txt2 position-absolute">Phiên bản đặc biệt</p>
                    <p class="promo-txt3 position-absolute cursor-pointer"><a href="{{ route('prodcut_detail.list')}}">XEM NGAY</a>
                    </p>
                </div>
            </div>

            <div class="promotion2 position-relative">
                <img src="{{asset('/templates/web/')}}/images/carousel2.jpg" alt="">
                <div class="promo2-box position-absolute">
                    <p class="promo-txt1 position-absolute">Garmin Vívosmart HR</p>
                    <p class="promo-txt2 position-absolute">Xu hướng của năm</p>
                    <p class="promo-txt3 position-absolute cursor-pointer"><a href="{{ route('prodcut_detail.list')}}">XEM NGAY</a>
                    </p>
                </div>
            </div>

            <div class="promotion3 position-relative">
                <img src="{{asset('/templates/web/')}}/images/carousel3.jpg" alt="">
                <div class="promo3-box position-absolute">
                    <p class="promo-txt1 position-absolute">Fitbit Flex Wireless Activity</p>
                    <p class="promo-txt2 position-absolute">Dành cho người năng động</p>
                    <p class="promo-txt3 position-absolute cursor-pointer"><a href="{{ route('prodcut_detail.list')}}">XEM NGAY</a>
                    </p>
                </div>
            </div>

        </aside>
        <!-- ===== END OF BIG CAROUSEL PROMOTION ===== -->

        <div class="homepage">
            <!-- ===== COLLECTIONS ===== -->
            <div class="collection d-flex wow fadeIn">

                <div class="animate cursor-pointer position-relative collection1">
                    <a href="{{ route('list_of_product.list')}}">
                        <div class="collection-item position-absolute">
                            <div class="collection-img position-absolute animate">
                                <img src="{{asset('/templates/web/')}}/images/3r.jpg"
                                    alt="Activity Trackers">
                            </div>
                            <div class="collection-txt position-absolute">
                                <p class="text-white m-auto">ACTIVITY<br>TRACKERS</p>
                            </div>
                            <div class="collection-cover animate position-absolute"></div>
                        </div>
                    </a>
                </div>

                <div class="animate cursor-pointer position-relative collection2">
                    <a href="{{ route('list_of_product.list')}}">
                        <div class="collection-item position-absolute">
                            <div class="collection-img position-absolute animate">
                                <img src="{{asset('/templates/web/')}}/images/3r1.jpg" alt="Sport Watches">
                            </div>
                            <div class="collection-txt position-absolute">
                                <p class="text-white m-auto">SPORT<br>WATCHES</p>
                            </div>
                            <div class="collection-cover animate position-absolute"></div>
                        </div>
                    </a>
                </div>

                <div class="animate cursor-pointer position-relative collection3">
                    <a href="{{ route('list_of_product.list')}}">
                        <div class="collection-item position-absolute">
                            <div class="collection-img position-absolute animate">
                                <img src="{{asset('/templates/web/')}}/images/2r.jpg" alt="Smart Watches">
                            </div>
                            <div class="collection-txt position-absolute">
                                <p class="text-white m-auto">SMART<br>WATCHES</p>
                            </div>
                            <div class="collection-cover animate position-absolute"></div>
                        </div>
                    </a>
                </div>

            </div>
            <!-- ===== END OF COLLECTIONS ===== -->

            <!-- ===== MODAL THÊM VÀO GIỎ HÀNG ==== -->
            <div class="modal fade" id="modal-cart" tabindex="-1" role="dialog" aria-labelledby="modal-w1-i1-label"
                aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">

                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h5 class="modal-title">THÊM VÀO GIỎ HÀNG</h5>
                        </div>

                        <div class="modal-body d-flex">
                            <div class="modal-img">
                                <img class="border"
                                    src="{{asset('/templates/web/')}}/images/3r.jpg" alt="" />
                            </div>
                            <div class="modal-info">
                                <p><a href="{{ route('prodcut_detail.list')}}">Fitbit Blaze Smart Fitness Watch</a></p>
                                <div class="modal-price mt-2">Giá: <span>1.000.000<sup>đ</sup></span></div>
                                <div class="modal-qty mt-3">Số lượng: <span style="font-weight: bold;">1</span></div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-muted" data-dismiss="modal">Tiếp tục mua hàng</button>
                            <button type="button" class="btn btn-success"
                                onclick="window.location.href = 'cart.html';">Xem giỏ hàng</button>
                        </div>

                    </div>
                </div>
            </div>
            <!-- END OF MODAL THÊM VÀO GIỎ HÀNG -->

            <!-- ===== WRAP 1 AREA ===== -->
            <div class="wrap1 wow fadeIn">
                <h3 class="wow fadeInLeft text-center" data-wow-delay="0.2s">Giảm Giá</h3>
                <div class="wrap-slider">

                    <div class="wrap-item position-relative hidden-border animate">
                        <a href="{{ route('prodcut_detail.list')}}">
                            <div class="wrap-img position-absolute cursor-pointer">
                                <img src="{{asset('/templates/web/')}}/images/3r1.jpg"
                                    alt="First slide" />
                            </div>
                        </a>
                        <!-- <div class="promo-symbol position-absolute animate">
                <p class="position-absolute text-light">SALE</p>
              </div> -->
                        <i class="fas fa-cart-plus animate position-absolute cursor-pointer" data-toggle="modal"
                            data-target="#modal-cart"></i>

                        <div class="info position-absolute">
                            <p class="price">1.000.000<sup>đ</sup></p>
                            <p class="old-price">1.200.000<sup>đ</sup></p>
                        </div>
                        <div class="product-name position-absolute">Fitbit Blaze Smart Fitness</div>
                    </div>

                    <div class="wrap-item position-relative hidden-border animate">
                        <a href="{{ route('prodcut_detail.list')}}">
                            <div class="wrap-img position-absolute">
                                <img src="{{asset('/templates/web/')}}/images/3r1.jpg" alt="Second slide" />
                            </div>
                        </a>
                        <!-- <div class="promo-symbol position-absolute animate">
                <p class="position-absolute text-light">SALE</p>
              </div> -->
                        <i class="fas fa-cart-plus animate position-absolute cursor-pointer" data-toggle="modal"
                            data-target="#modal-cart"></i>
                        <div class="info position-absolute">
                            <p class="price">2.100.000<sup>đ</sup></p>
                            <p class="old-price">2.500.000<sup>đ</sup></p>
                        </div>
                        <div class="product-name position-absolute">Garmin vívosmart HR</div>
                    </div>

                    <div class="wrap-item position-relative hidden-border animate">
                        <a href="{{ route('prodcut_detail.list')}}">
                            <div class="wrap-img position-absolute">
                                <img src="{{asset('/templates/web/')}}/images/3r1.jpg" alt="Second slide" />
                            </div>
                        </a>
                        <!-- <div class="promo-symbol position-absolute animate">
                  <p class="position-absolute text-light">SALE</p>
                </div> -->
                        <i class="fas fa-cart-plus animate position-absolute cursor-pointer" data-toggle="modal"
                            data-target="#modal-cart"></i>

                        <div class="info position-absolute">
                            <p class="price">5.000.000<sup>đ</sup></p>
                            <p class="old-price">6.000.000<sup>đ</sup></p>
                        </div>
                        <div class="product-name position-absolute">Fossil Q Gen 3 Smartwatch</div>
                    </div>

                    <div class="wrap-item position-relative hidden-border animate">
                        <a href="{{ route('prodcut_detail.list')}}">
                            <div class="wrap-img position-absolute">
                                <img src="{{asset('/templates/web/')}}/images/3r1.jpg"
                                    alt="Second slide" />
                            </div>
                        </a>
                        <!-- <div class="promo-symbol position-absolute animate">
                  <p class="position-absolute text-light">SALE</p>
                </div> -->
                        <i class="fas fa-cart-plus animate position-absolute cursor-pointer" data-toggle="modal"
                            data-target="#modal-cart"></i>

                        <div class="info position-absolute">
                            <p class="price">1.900.000<sup>đ</sup></p>
                            <p class="old-price">2.100.000<sup>đ</sup></p>
                        </div>
                        <div class="product-name position-absolute">Samsung Gear Sport</div>
                    </div>

                    <div class="wrap-item position-relative hidden-border animate">
                        <a href="{{ route('prodcut_detail.list')}}">
                            <div class="wrap-img position-absolute">
                                <img src="{{asset('/templates/web/')}}/images/3r1.jpg" alt="Second slide" />
                            </div>
                        </a>
                        <!-- <div class="promo-symbol position-absolute animate">
                  <p class="position-absolute text-light">SALE</p>
                </div> -->
                        <i class="fas fa-cart-plus animate position-absolute cursor-pointer" data-toggle="modal"
                            data-target="#modal-cart"></i>

                        <div class="info position-absolute">
                            <p class="price">1.000.000<sup>đ</sup></p>
                            <p class="old-price">1.250.000<sup>đ</sup></p>
                        </div>
                        <div class="product-name position-absolute">Apple Watch Series 3</div>
                    </div>

                    <div class="wrap-item position-relative hidden-border animate">
                        <a href="{{ route('prodcut_detail.list')}}">
                            <div class="wrap-img position-absolute">
                                <img src="{{asset('/templates/web/')}}/images/3r1.jpg" alt="Second slide" />
                            </div>
                        </a>
                        <!-- <div class="promo-symbol position-absolute animate">
                  <p class="position-absolute text-light">SALE</p>
                </div> -->
                        <i class="fas fa-cart-plus animate position-absolute cursor-pointer" data-toggle="modal"
                            data-target="#modal-cart"></i>

                        <div class="info position-absolute">
                            <p class="price">3.000.000<sup>đ</sup></p>
                            <p class="old-price">4.000.000<sup>đ</sup></p>
                        </div>
                        <div class="product-name position-absolute">Garmin Forerunner 235</div>
                    </div>

                </div>
            </div>
            <!-- ===== END OF WRAP 1 AREA ===== -->

            <!-- ===== WRAP 2 AREA ===== -->
            <div class="wrap2 wow fadeIn">
                <h3 class="wow fadeInLeft text-center" data-wow-delay="0.2s">Bán Chạy</h3>
                <div class="wrap-slider">

                    <div class="wrap-item position-relative hidden-border animate">
                        <a href="{{ route('prodcut_detail.list')}}">
                            <div class="wrap-img position-absolute">
                                <img src="{{asset('/templates/web/')}}/images/3r1.jpg"
                                    alt="First slide" />
                            </div>
                        </a>
                        <!-- <div class="promo-symbol position-absolute animate">
                <p class="position-absolute text-light">SALE</p>
              </div> -->
                        <i class="fas fa-cart-plus animate position-absolute cursor-pointer" data-toggle="modal"
                            data-target="#modal-cart"></i>

                        <div class="info position-absolute">
                            <p class="price">1.000.000<sup>đ</sup></p>
                            <!-- <p class="old-price">1.200.000<sup>đ</sup></p> -->
                        </div>
                        <div class="product-name position-absolute">Fitbit Surge Fitness</div>
                    </div>

                    <div class="wrap-item position-relative hidden-border animate">
                        <a href="{{ route('prodcut_detail.list')}}">
                            <div class="wrap-img position-absolute">
                                <img src="{{asset('/templates/web/')}}/images/3r1.jpg"
                                    alt="Second slide" />
                            </div>
                        </a>
                        <div class="promo-symbol position-absolute animate">
                            <p class="position-absolute text-light">SALE</p>
                        </div>
                        <i class="fas fa-cart-plus animate position-absolute cursor-pointer" data-toggle="modal"
                            data-target="#modal-cart"></i>

                        <div class="info position-absolute">
                            <p class="price">2.100.000<sup>đ</sup></p>
                            <p class="old-price">2.500.000<sup>đ</sup></p>
                        </div>
                        <div class="product-name position-absolute">Garmin Forerunner 935</div>
                    </div>

                    <div class="wrap-item position-relative hidden-border animate">
                        <a href="{{ route('prodcut_detail.list')}}">
                            <div class="wrap-img position-absolute">
                                <img src="{{asset('/templates/web/')}}/images/3r1.jpg" alt="Second slide" />
                            </div>
                        </a>
                        <!-- <div class="promo-symbol position-absolute animate">
                <p class="position-absolute text-light">SALE</p>
              </div> -->
                        <i class="fas fa-cart-plus animate position-absolute cursor-pointer" data-toggle="modal"
                            data-target="#modal-cart"></i>

                        <div class="info position-absolute">
                            <p class="price">5.000.000<sup>đ</sup></p>
                            <!-- <p class="old-price">6.000.000<sup>đ</sup></p> -->
                        </div>
                        <div class="product-name position-absolute">Fitbit Versa</div>
                    </div>

                    <div class="wrap-item position-relative hidden-border animate">
                        <a href="{{ route('prodcut_detail.list')}}">
                            <div class="wrap-img position-absolute">
                                <img src="{{asset('/templates/web/')}}/images/3r1.jpg" alt="Second slide" />
                            </div>
                        </a>
                        <div class="promo-symbol position-absolute animate">
                            <p class="position-absolute text-light">SALE</p>
                        </div>
                        <i class="fas fa-cart-plus animate position-absolute cursor-pointer" data-toggle="modal"
                            data-target="#modal-cart"></i>

                        <div class="info position-absolute">
                            <p class="price">1.900.000<sup>đ</sup></p>
                            <p class="old-price">2.100.000<sup>đ</sup></p>
                        </div>
                        <div class="product-name position-absolute">Garmin vívomove HR</div>
                    </div>

                    <div class="wrap-item position-relative hidden-border animate">
                        <a href="{{ route('prodcut_detail.list')}}">
                            <div class="wrap-img position-absolute">
                                <img src="{{asset('/templates/web/')}}/images/3r1.jpg" alt="Second slide" />
                            </div>
                        </a>
                        <!-- <div class="promo-symbol position-absolute animate">
                <p class="position-absolute text-light">SALE</p>
              </div> -->
                        <i class="fas fa-cart-plus animate position-absolute cursor-pointer" data-toggle="modal"
                            data-target="#modal-cart"></i>

                        <div class="info position-absolute">
                            <p class="price">1.000.000<sup>đ</sup></p>
                            <!-- <p class="old-price">1.250.000<sup>đ</sup></p> -->
                        </div>
                        <div class="product-name position-absolute">Apple Watch Series 3</div>
                    </div>

                    <div class="wrap-item position-relative hidden-border animate">
                        <a href="{{ route('prodcut_detail.list')}}">
                            <div class="wrap-img position-absolute">
                                <img src="{{asset('/templates/web/')}}/images/3r1.jpg" alt="Second slide" />
                            </div>
                        </a>
                        <div class="promo-symbol position-absolute animate">
                            <p class="position-absolute text-light">SALE</p>
                        </div>
                        <i class="fas fa-cart-plus animate position-absolute cursor-pointer" data-toggle="modal"
                            data-target="#modal-cart"></i>

                        <div class="info position-absolute">
                            <p class="price">3.000.000<sup>đ</sup></p>
                            <p class="old-price">4.000.000<sup>đ</sup></p>
                        </div>
                        <div class="product-name position-absolute">Garmin Forerunner 235</div>
                    </div>

                </div>
            </div>
            <!-- ===== END OF WRAP 2 AREA ===== -->

            <!-- ===== ADVERTISEMENT ===== -->
            <div class="advert wow fadeIn position-relative">
                <a href="promotion.html">
                    <div class="position-absolute ad-box">
                        <p class="ad-txt1 position-absolute text-white">SPRING SALE</p>
                        <P class="ad-txt2 position-absolute color-C3C3CE">GIẢM ĐẾN <span class="text-white">50%</span>
                        </P>
                    </div>
                </a>
            </div>
            <!-- ===== END OF ADVERTISEMENT ===== -->

            <!-- ===== WRAP 3 AREA ===== -->
            <div class="wrap3 wow fadeIn">
                <h3 class="wow fadeInLeft text-center" data-wow-delay="0.2s">Hàng Mới Về</h3>
                <div class="wrap-slider">

                    <div class="wrap-item position-relative hidden-border animate">
                        <a href="{{ route('prodcut_detail.list')}}">
                            <div class="wrap-img position-absolute">
                                <img src="{{asset('/templates/web/')}}/images/3r1.jpg"
                                    alt="First slide" />
                            </div>
                        </a>
                        <!-- <div class="promo-symbol position-absolute animate">
                  <p class="position-absolute text-light">SALE</p>
                </div> -->
                        <i class="fas fa-cart-plus animate position-absolute cursor-pointer" data-toggle="modal"
                            data-target="#modal-cart"></i>

                        <div class="info position-absolute">
                            <p class="price">1.000.000<sup>đ</sup></p>
                            <!-- <p class="old-price">1.200.000<sup>đ</sup></p> -->
                        </div>
                        <div class="product-name position-absolute">Fitbit Blaze Smart Fitness</div>
                    </div>

                    <div class="wrap-item position-relative hidden-border animate">
                        <a href="{{ route('prodcut_detail.list')}}">
                            <div class="wrap-img position-absolute">
                                <img src="{{asset('/templates/web/')}}/images/3r1.jpg" alt="Second slide" />
                            </div>
                        </a>
                        <!-- <div class="promo-symbol position-absolute animate">
                  <p class="position-absolute text-light">SALE</p>
                </div> -->
                        <i class="fas fa-cart-plus animate position-absolute cursor-pointer" data-toggle="modal"
                            data-target="#modal-cart"></i>

                        <div class="info position-absolute">
                            <p class="price">2.100.000<sup>đ</sup></p>
                            <!-- <p class="old-price">2.500.000<sup>đ</sup></p> -->
                        </div>
                        <div class="product-name position-absolute">Garmin vívosmart HR</div>
                    </div>

                    <div class="wrap-item position-relative hidden-border animate">
                        <a href="{{ route('prodcut_detail.list')}}">
                            <div class="wrap-img position-absolute">
                                <img src="{{asset('/templates/web/')}}/images/3r1.jpg" alt="Second slide" />
                            </div>
                        </a>
                        <!-- <div class="promo-symbol position-absolute animate">
                  <p class="position-absolute text-light">SALE</p>
                </div> -->
                        <i class="fas fa-cart-plus animate position-absolute cursor-pointer" data-toggle="modal"
                            data-target="#modal-cart"></i>

                        <div class="info position-absolute">
                            <p class="price">5.000.000<sup>đ</sup></p>
                            <!-- <p class="old-price">6.000.000<sup>đ</sup></p> -->
                        </div>
                        <div class="product-name position-absolute">Fossil Q Gen 3 Smartwatch</div>
                    </div>

                    <div class="wrap-item position-relative hidden-border animate">
                        <a href="{{ route('prodcut_detail.list')}}">
                            <div class="wrap-img position-absolute">
                                <img src="{{asset('/templates/web/')}}/images/3r1.jpg"
                                    alt="Second slide" />
                            </div>
                        </a>
                        <!-- <div class="promo-symbol position-absolute animate">
                  <p class="position-absolute text-light">SALE</p>
                </div> -->
                        <i class="fas fa-cart-plus animate position-absolute cursor-pointer" data-toggle="modal"
                            data-target="#modal-cart"></i>

                        <div class="info position-absolute">
                            <p class="price">1.900.000<sup>đ</sup></p>
                            <!-- <p class="old-price">2.100.000<sup>đ</sup></p> -->
                        </div>
                        <div class="product-name position-absolute">Samsung Gear Sport</div>
                    </div>

                    <div class="wrap-item position-relative hidden-border animate">
                        <a href="{{ route('prodcut_detail.list')}}">
                            <div class="wrap-img position-absolute">
                                <img src="{{asset('/templates/web/')}}/images/3r1.jpg" alt="Second slide" />
                            </div>
                        </a>
                        <!-- <div class="promo-symbol position-absolute animate">
                  <p class="position-absolute text-light">SALE</p>
                </div> -->
                        <i class="fas fa-cart-plus animate position-absolute cursor-pointer" data-toggle="modal"
                            data-target="#modal-cart"></i>

                        <div class="info position-absolute">
                            <p class="price">1.000.000<sup>đ</sup></p>
                            <!-- <p class="old-price">1.250.000<sup>đ</sup></p> -->
                        </div>
                        <div class="product-name position-absolute">Apple Watch Series 3</div>
                    </div>

                    <div class="wrap-item position-relative hidden-border animate">
                        <a href="{{ route('prodcut_detail.list')}}">
                            <div class="wrap-img position-absolute">
                                <img src="{{asset('/templates/web/')}}/images/3r1.jpg" alt="Second slide" />
                            </div>
                        </a>
                        <!-- <div class="promo-symbol position-absolute animate">
                  <p class="position-absolute text-light">SALE</p>
                </div> -->
                        <i class="fas fa-cart-plus animate position-absolute cursor-pointer" data-toggle="modal"
                            data-target="#modal-cart"></i>

                        <div class="info position-absolute">
                            <p class="price">3.000.000<sup>đ</sup></p>
                            <!-- <p class="old-price">4.000.000<sup>đ</sup></p> -->
                        </div>
                        <div class="product-name position-absolute">Garmin Forerunner 235</div>
                    </div>

                </div>
            </div>
            <!-- ===== END OF WRAP 3 AREA ===== -->

            <!-- ===== WRAP 4 AREA ===== -->
            <div class="wrap4 wow fadeIn">
                <h3 class="wow fadeInLeft text-center" data-wow-delay="0.2s">Xu Hướng</h1>
                    <div class="wrap-slider">

                        <div class="wrap-item position-relative hidden-border animate">
                            <a href="{{ route('prodcut_detail.list')}}">
                                <div class="wrap-img position-absolute">
                                    <img src="{{asset('/templates/web/')}}/images/3r1.jpg"
                                        alt="First slide" />
                                </div>
                            </a>
                            <div class="promo-symbol position-absolute animate">
                                <p class="position-absolute text-light">SALE</p>
                            </div>
                            <i class="fas fa-cart-plus animate position-absolute cursor-pointer" data-toggle="modal"
                                data-target="#modal-cart"></i>

                            <div class="info position-absolute">
                                <p class="price">1.000.000<sup>đ</sup></p>
                                <p class="old-price">1.200.000<sup>đ</sup></p>
                            </div>
                            <div class="product-name position-absolute">Fitbit Surge Fitness</div>
                        </div>

                        <div class="wrap-item position-relative hidden-border animate">
                            <a href="{{ route('prodcut_detail.list')}}">
                                <div class="wrap-img position-absolute">
                                    <img src="{{asset('/templates/web/')}}/images/3r1.jpg"
                                        alt="Second slide" />
                                </div>
                            </a>
                            <!-- <div class="promo-symbol position-absolute animate">
                                    <p class="position-absolute text-light">SALE</p>
                                  </div> -->
                            <i class="fas fa-cart-plus animate position-absolute cursor-pointer" data-toggle="modal"
                                data-target="#modal-cart"></i>

                            <div class="info position-absolute">
                                <p class="price">2.100.000<sup>đ</sup></p>
                                <!-- <p class="old-price">2.500.000<sup>đ</sup></p> -->
                            </div>
                            <div class="product-name position-absolute">Garmin Forerunner 935</div>
                        </div>

                        <div class="wrap-item position-relative hidden-border animate">
                            <a href="{{ route('prodcut_detail.list')}}">
                                <div class="wrap-img position-absolute">
                                    <img src="{{asset('/templates/web/')}}/images/3r1.jpg"
                                        alt="Second slide" />
                                </div>
                            </a>
                            <div class="promo-symbol position-absolute animate">
                                <p class="position-absolute text-light">SALE</p>
                            </div>
                            <i class="fas fa-cart-plus animate position-absolute cursor-pointer" data-toggle="modal"
                                data-target="#modal-cart"></i>

                            <div class="info position-absolute">
                                <p class="price">5.000.000<sup>đ</sup></p>
                                <p class="old-price">6.000.000<sup>đ</sup></p>
                            </div>
                            <div class="product-name position-absolute">Fitbit Versa</div>
                        </div>

                        <div class="wrap-item position-relative hidden-border animate">
                            <a href="{{ route('prodcut_detail.list')}}">
                                <div class="wrap-img position-absolute">
                                    <img src="{{asset('/templates/web/')}}/images/3r1.jpg" alt="Second slide" />
                                </div>
                            </a>
                            <!-- <div class="promo-symbol position-absolute animate">
                                    <p class="position-absolute text-light">SALE</p>
                                  </div> -->
                            <i class="fas fa-cart-plus animate position-absolute cursor-pointer" data-toggle="modal"
                                data-target="#modal-cart"></i>

                            <div class="info position-absolute">
                                <p class="price">1.900.000<sup>đ</sup></p>
                                <!-- <p class="old-price">2.100.000<sup>đ</sup></p> -->
                            </div>
                            <div class="product-name position-absolute">Garmin vívomove HR</div>
                        </div>

                        <div class="wrap-item position-relative hidden-border animate">
                            <a href="{{ route('prodcut_detail.list')}}">
                                <div class="wrap-img position-absolute">
                                    <img src="{{asset('/templates/web/')}}/images/3r1.jpg" alt="Second slide" />
                                </div>
                            </a>
                            <div class="promo-symbol position-absolute animate">
                                <p class="position-absolute text-light">SALE</p>
                            </div>
                            <i class="fas fa-cart-plus animate position-absolute cursor-pointer" data-toggle="modal"
                                data-target="#modal-cart"></i>

                            <div class="info position-absolute">
                                <p class="price">1.000.000<sup>đ</sup></p>
                                <p class="old-price">1.250.000<sup>đ</sup></p>
                            </div>
                            <div class="product-name position-absolute">Apple Watch Series 3</div>
                        </div>

                        <div class="wrap-item position-relative hidden-border animate">
                            <a href="{{ route('prodcut_detail.list')}}">
                                <div class="wrap-img position-absolute">
                                    <img src="{{asset('/templates/web/')}}/images/3r1.jpg" alt="Second slide" />
                                </div>
                            </a>
                            <!-- <div class="promo-symbol position-absolute animate">
                                    <p class="position-absolute text-light">SALE</p>
                                  </div> -->
                            <i class="fas fa-cart-plus animate position-absolute cursor-pointer" data-toggle="modal"
                                data-target="#modal-cart"></i>

                            <div class="info position-absolute">
                                <p class="price">3.000.000<sup>đ</sup></p>
                                <!-- <p class="old-price">4.000.000<sup>đ</sup></p> -->
                            </div>
                            <div class="product-name position-absolute">Garmin Forerunner 235</div>
                        </div>

                    </div>
            </div>
            <!-- ===== END OF WRAP 4 AREA ===== -->

            </section>

            <!-- ===== FOOTER ===== -->
            @include('Camera.footer')

            <div class="back2top hide-item position-fixed">
                <i class="fas fa-chevron-circle-up"></i>
            </div>

        </div>
        @endsection