@extends('Camera.master')
@section('content')
@include('Camera.header')
<section class="promotion-page">
    
        <!-- ===== BANNER ===== -->
        <div class="advert wow fadeIn position-relative">
            <a href="promotion.html">
              <div class="position-absolute ad-box">
                <p class="ad-txt1 position-absolute text-white">SPRING SALE</p>
                <P class="ad-txt2 position-absolute color-C3C3CE">GIẢM ĐẾN <span class="text-white">50%</span></P>
              </div>
            </a>
          </div>
        <!-- ===== END OF BANNER ===== -->
  
        <!-- ===== BREADCRUMB ===== -->
        <div class="breadcrumb-lop">
          <a href="index.html">Trang chủ</a> > Khuyến mại
        </div>
        <!-- ===== END OF BREADCRUMB ===== -->

        <!-- ===== PROMO ITEMS ===== -->
        <a href="list_of_products.html">
          <div class="promo1 wow fadeIn position-relative">
            <div class="position-absolute ad-img">
              <img src="{{asset('/templates/web/')}}/images/promo1.jpg" alt="Advertisement Image">
            </div>
            <!-- <p class="position-absolute ad-txt1">GIẢM GIÁ</p> -->
            <p class="position-absolute ad-txt2">GIẢM ĐẾN <span>40%</span></p>
            <div class="position-absolute ad-box">
              <p class="ad-txt3 text-white">GARMIN WATCH</p>
            </div>
          </div>
        </a>
        
        <a href="list_of_products.html">
          <div class="promo2 wow fadeIn position-relative">
            <div class="position-absolute ad-img">
              <img src="{{asset('/templates/web/')}}/images/promo2.jpg" alt="Advertisement Image">
            </div>
            <div class="position-absolute ad-box1">
              <i class="position-absolute fab fa-apple"></i>
              <p class="position-absolute ad-txt1">APPLE WATCH 4</p>
            </div>
            <div class="position-absolute ad-box2">
              <p class="ad-txt2">MUA <span>1</span> TẶNG <span>1</span></p>
            </div>
          </div>
        </a>
        
        <a href="list_of_products.html">
          <div class="promo3 wow fadeIn position-relative">
            <div class="position-absolute ad-img">
              <img src="{{asset('/templates/web/')}}/images/promo3.jpg" alt="Advertisement Image">
            </div>
            <div class="position-absolute ad-box">
              <p class="position-absolute ad-txt1 text-light">GIẢM GIÁ <span>10%</span></p>
              <p class="position-absolute ad-txt2 text-light">CHO MỘT THƯƠNG HIỆU BẤT KỲ*</p>
              <p class="position-absolute ad-txt3 text-light">VÀO 11:00 - 12:00 SÁNG THỨ HAI HÀNG TUẦN</p>
              <P class="position-absolute ad-txt4">* Thương hiệu được chọn giảm giá ngẫu nhiên mỗi tuần</P>
            </div>
          </div>
        </a>
        <!-- ===== END OF PROMO ITEMS ===== -->

      </section>
      <!-- ===== FOOTER ===== -->
      @include('Camera.footer')

      <div class="back2top hide-item position-fixed">
        <i class="fas fa-chevron-circle-up"></i>
      </div>
@endsection