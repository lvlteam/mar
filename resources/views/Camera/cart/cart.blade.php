@extends('Camera.master')
@section('content')
@include('Camera.header')
<section class="cart-page">
    
        <!-- ===== BANNER ===== -->
        <div class="advert wow fadeIn position-relative">
            <a href="promotion.html">
              <div class="position-absolute ad-box">
                <p class="ad-txt1 position-absolute text-white">SPRING SALE</p>
                <P class="ad-txt2 position-absolute color-C3C3CE">GIẢM ĐẾN <span class="text-white">50%</span></P>
              </div>
            </a>
          </div>
        <!-- ===== END OF BANNER ===== -->
  
        <!-- ===== BREADCRUMB ===== -->
        <div class="breadcrumb-lop">
          <a href="index.html">Trang chủ</a> > Giỏ hàng
        </div>
        <!-- ===== END OF BREADCRUMB ===== -->

        <div class="cart-steps">
          <div class="cart-step1">
            <p>GIỎ HÀNG</p>
          </div>

          <div class="cart-step2">
            <p>ĐẶT HÀNG</p>
          </div>
        </div>

        <div class="cart-table">
          <table>
            <tr>
              <th><input type="checkbox" name="cart" value="all" checked></th>
              <th></th>
              <th>Sản phẩm</th>
              <th>Giá</th>
              <th><span>SL</span><span>Số lượng</span></th>
              <th>Thành tiền</th>
              <th></th>
            </tr>
            <tr>
              <td><input type="checkbox" name="cart" value="all" checked></td>
              <td><img src="{{asset('/templates/web/')}}/images/3r.jpg" alt=""></td>
              <td>Fitbit Blaze Smart Fitness</td>
              <td>4.000.000<sup>đ</sup></td>
              <td><input type="number" min="1" max="9" value="1"></td>
              <td>4.000.000<sup>đ</sup></td>
              <td><a href="#"><i class="far fa-trash-alt"></i></a></td>
            </tr>
            <tr>
              <td><input type="checkbox" name="cart" value="all" checked></td>
              <td><img src="{{asset('/templates/web/')}}/images/3r.jpg" alt=""></td>
              <td>Samsung Gear Sport (SM-R600)</td>
              <td><span>6.000.000<sup>đ</sup></span> 5.000.000<sup>đ</sup></td>
              <td><input type="number" min="1" max="9" value="1"></td>
              <td>5.000.000<sup>đ</sup></td>
              <td><a href="#"><i class="far fa-trash-alt"></i></a></td>
            </tr>
            
          </table>

          <div class="cart-sum-box d-flex">
              <div class="cart-sum-box1">
                  <span class="cart-sum-type">Nhập mã giảm giá (nếu có)</span>
                  <span class="cart-sum-input"><input type="text" placeholder=""></span>
                  <span class="cart-sum-input"><input class="cursor-pointer" type="button" value="Áp dụng"></span>
                </div>

              <div class="cart-sum-box2">
                <span class="cart-sum-total">TỔNG TIỀN</span>
                <span class="cart-sum-number">9.000.000 đ</span>
              </div>
          </div>

          <div class="cart-next">
            <a href="{{route('order.list')}}"> <input class="cursor-pointer" type="button" value="Trang Kế Tiếp >"></a>
          </div>
  
        </div>

      </section>
<!-- ===== FOOTER ===== -->
@include('Camera.footer')

<div class="back2top hide-item position-fixed">
  <i class="fas fa-chevron-circle-up"></i>
</div>
@endsection