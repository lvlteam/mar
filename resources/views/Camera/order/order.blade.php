@extends('Camera.master')
@section('content')
@include('Camera.header')
<section class="order-page">
    
        <!-- ===== BANNER ===== -->
        <div class="advert wow fadeIn position-relative">
            <a href="promotion.html">
              <div class="position-absolute ad-box">
                <p class="ad-txt1 position-absolute text-white">SPRING SALE</p>
                <P class="ad-txt2 position-absolute color-C3C3CE">GIẢM ĐẾN <span class="text-white">50%</span></P>
              </div>
            </a>
          </div>
        <!-- ===== END OF BANNER ===== -->
  
        <!-- ===== BREADCRUMB ===== -->
        <div class="breadcrumb-lop">
          <a href="index.html">Trang chủ</a> > Giỏ hàng
        </div>
        <!-- ===== END OF BREADCRUMB ===== -->

        <div class="cart-steps">
          <div class="cart-step1">
            <p>GIỎ HÀNG</p>
          </div>

          <div class="order-step2">
            <p>ĐẶT HÀNG</p>
          </div>
        </div>

        <div class="order-wrap">
        
          <div class="order-methods">
            <p>Thanh toán</p>
          </div>

          <div class="order-info">
            <p>1. Vui lòng nhập thông tin của Quý khách</p>
            <form action="submit">
              <input type="text" placeholder="Họ và tên*">
              <input type="text" placeholder="Số điện thoại*">
              <input type="text" placeholder="Địa chỉ nhận hàng*">
              <p>*Trường bắt buộc phải nhập</p>
              <input class="order-note" type="text" placeholder="Ghi chú">
            </form>
            <br>
          </div>
        
          <div class="order-payment">
            <p>2. Chọn hình thức thanh toán</p>
            <div>
              <input type="radio" name="payment" value="bank" checked> Chuyển khoản ngân hàng. Vui lòng chuyển tiền vào số tài
              khoản của chúng tôi bên dưới:
              <div>
                <p>Số TK: 0000 1234 6789 2468</p>
                <p>Chủ TK: Công ty TNHH CyWatch</p>
                <p>Ngân hàng Thương mại Cổ phần Việt Nam - Chi nhánh Hà Nội</p>
              </div>
              <p>Hoặc</p>
              <div>
                <p>Số TK: 0000 1234 6789 1357</p>
                <p>Chủ TK: Công ty TNHH CyWatch</p>
                <p>Ngân hàng Thương mại Cổ phần Nam Việt - Chi nhánh Hà Nội</p>
              </div>
            
              <input type="radio" name="payment" value="cash"> Giao hàng tại nhà, thanh toán cho đơn vị chuyển phát nhanh.
            
            </div>
          </div>
        
          <div class="order-button">
              <div class="order-back">
                  <a href="cart.html"> <input class="cursor-pointer" type="button" value="< Quay Lại"></a>
                </div>
              <div class="order-next">
                <a href="{{route('ordersuccess.list')}}"> <input class="cursor-pointer" type="button" value="Đặt Hàng >"></a>
              </div>
          </div>
        
        </div>

      </section>
<!-- ===== FOOTER ===== -->
@include('Camera.footer')

<div class="back2top hide-item position-fixed">
  <i class="fas fa-chevron-circle-up"></i>
</div>
@endsection