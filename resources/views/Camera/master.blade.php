<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="{{asset('/templates/web/')}}/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
        integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('/templates/web/')}}/css/slick.min.css" />
    <link rel="stylesheet" href="{{asset('/templates/web/')}}/css/slick-theme.min.css" />
    <link rel="stylesheet" href="{{asset('/templates/web/')}}/css/animate.min.css" />
    <link rel="stylesheet" href="{{asset('/templates/web/')}}/css/hover-min.css" />
    <link rel="stylesheet" href="{{asset('/templates/web/')}}/style.css" type="text/css" />
    <link
        href="https://fonts.googleapis.com/css?family=Muli|Quicksand|Oswald|Coiny|Dancing+Script|Josefin+Sans|Pacifico|Jura|Saira"
        rel="stylesheet" />
    <title>CyWatch</title>
</head>
<body >
<div class="_container">
@yield('content')
</div>

<script src="{{asset('/templates/web/')}}/js/jquery-3.3.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
            integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
            crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
            integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
            crossorigin="anonymous"></script>
        <script src="{{asset('/templates/web/')}}/js/slick.min.js"></script>
        <script src="{{asset('/templates/web/')}}/js/wow.min.js"></script>
        <script src="{{asset('/templates/web/')}}/script.js" type="text/javascript"></script>
        <!-- <script
            lang="javascript">var _vc_data = { id: 6764547, secret: 'a597fa606bf87daaeda67c989f0f2b45' }; (function () { var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true; ga.defer = true; ga.src = '//live.vnpgroup.net/client/tracking.js?id=6764547'; var s = document.getElementsByTagName('script'); s[0].parentNode.insertBefore(ga, s[0]); })();</script> -->
@yield('script')
@yield('footer')
</body>
</html>
