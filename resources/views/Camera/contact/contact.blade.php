@extends('Camera.master')
@section('content')
@include('Camera.header')
<section class="contact-page">
    
        <!-- ===== BANNER ===== -->
        <div class="advert wow fadeIn position-relative">
            <a href="promotion.html">
              <div class="position-absolute ad-box">
                <p class="ad-txt1 position-absolute text-white">SPRING SALE</p>
                <P class="ad-txt2 position-absolute color-C3C3CE">GIẢM ĐẾN <span class="text-white">50%</span></P>
              </div>
            </a>
          </div>
        <!-- ===== END OF BANNER ===== -->
  
        <!-- ===== BREADCRUMB ===== -->
        <div class="breadcrumb-lop">
          <a href="index.html">Trang chủ</a> > Giỏ hàng
        </div>
        <!-- ===== END OF BREADCRUMB ===== -->

        <!-- ===== CONTACT INFO ===== -->
        <div class="contact-info d-flex">
          <div class="contact-phone">
            <div class="contact-circle">
              <i class="fas fa-mobile-alt"></i>
            </div>
            <p class="contact-txt">0999 999 999</p>
          </div>

          <div class="contact-address">
            <div class="contact-circle">
              <i class="fas fa-map-marker-alt"></i>
            </div>
            <p class="contact-txt">Lorem Ipsum Dolorsit </p>
          </div>
          
          <div class="contact-email">
            <div class="contact-circle">
                <i class="fas fa-envelope"></i>
            </div>
            <p class="contact-txt">loremipsum@email.com</p>
          </div>
        </div>
        <!-- ===== END OF CONTACT INFO ===== -->

        <div class="contact-map">
          <img src="{{asset('/templates/web/')}}/images/disneyland-google-map.png" alt="Map">
        </div>

        <div class="contact-bank">
          <p class="contact-bank-txt1">THÔNG TIN CHUYỂN KHOẢN NGÂN HÀNG</p>
          <div>
              Số TK: 0000 1234 6789 2468 <br>
              Chủ TK: Công ty TNHH CyWatch <br>   
              Ngân hàng Thương mại Cổ phần Việt Nam - Chi nhánh Hà Nội
          </div>
          <span class="contact-bank-txt2">Hoặc</span>
          <div>
              Số TK: 0000 1234 6789 1357 <br>
              Chủ TK: Công ty TNHH CyWatch <br>   
              Ngân hàng Thương mại Cổ phần Nam Việt - Chi nhánh Hà Nội
          </div>
        </div>

        <div class="contact-us">
          <p>LIÊN HỆ VỚI CHÚNG TÔI</p>

          <div class="contact-form">
            <div class="contact-form-info">
              <input type="text" placeholder="Họ và tên*"><br>
              <input type="text" placeholder="Số điện thoại*"><br>
              <input type="text" placeholder="Email"><br>
              <p>* Trường bắt buộc phải nhập</p>
            </div>
            <div class="contact-form-message">
              <input type="text" placeholder="Vui lòng nhập tin nhắn ở đây">
            </div>
          </div>

          <div class="contact-next">
              <a href="{{route('send.list')}}"> <input class="cursor-pointer" type="button" value="Gửi Tin Nhắn"></a>
          </div>
        </div>


      </section>
 <!-- ===== FOOTER ===== -->
 @include('Camera.footer')

<div class="back2top hide-item position-fixed">
  <i class="fas fa-chevron-circle-up"></i>
</div>
@endsection