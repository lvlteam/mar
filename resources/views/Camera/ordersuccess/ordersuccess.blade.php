@extends('Camera.master')
@section('content')
@include('Camera.header')
<section class="order-success-page">
    
        <!-- ===== BANNER ===== -->
        <div class="advert wow fadeIn position-relative">
            <a href="{{route('promotion.list')}}">
              <div class="position-absolute ad-box">
                <p class="ad-txt1 position-absolute text-white">SPRING SALE</p>
                <P class="ad-txt2 position-absolute color-C3C3CE">GIẢM ĐẾN <span class="text-white">50%</span></P>
              </div>
            </a>
          </div>
        <!-- ===== END OF BANNER ===== -->
  
        <!-- ===== BREADCRUMB ===== -->
        <div class="breadcrumb-lop">
          <a href="{{route('camera.home.list')}}">Trang chủ</a> > Giỏ hàng
        </div>
        <!-- ===== END OF BREADCRUMB ===== -->

        <div class="order-success-wrap wow fadeIn">
          <p>Chúc mừng Quý khách đã đặt hàng thành công.<br>Chúng tôi sẽ liên hệ với Quý khách trong thời gian sớm nhất.
              Cảm ơn đã lựa chọn và tin tưởng sản phẩm của chúng tôi!</p>
          <p>Nếu sau <span id="order-success-countdown">9</span> giây không tự chuyển về trang chủ, Quý khách vui lòng bấm <a href="index.html">vào đây</a>.</p>
        </div>

      </section>
<!-- ===== FOOTER ===== -->
@include('Camera.footer')

<div class="back2top hide-item position-fixed">
  <i class="fas fa-chevron-circle-up"></i>
</div>
@endsection