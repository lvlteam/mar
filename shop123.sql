/*
SQLyog Community v13.0.1 (64 bit)
MySQL - 10.1.38-MariaDB : Database - camera_shop
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`camera_shop` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;

USE `camera_shop`;

/*Table structure for table `user_group` */

DROP TABLE IF EXISTS `user_group`;

CREATE TABLE `user_group` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permission` text COLLATE utf8mb4_unicode_ci,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `user_group` */

insert  into `user_group`(`id`,`name`,`permission`,`status`,`created_at`,`updated_at`) values 
(0,'Administrator','[\"admin\",\"admin.user.add\",\"admin.user.edit\",\"admin.user.list\",\"admin.user.save\",\"admin.user.update\",\"admin.user.delete\",\"admin.user.get_profile\",\"admin.user.save_profile\",\"admin.group.list\",\"admin.group.add\",\"admin.group.edit\",\"admin.group.permission\",\"admin.group.delete\",\"admin.thong_ke.tinh_hinh_ke_khai_theo_nhom_nganh\",\"admin.thong_ke.ke-khai-mhkd-tai-cho-truyen-thong\",\"admin.ho_kinh_doanh.list\",\"admin.ho_kinh_doanh.add\",\"admin.ho_kinh_doanh.edit\",\"admin.ho_kinh_doanh.update\",\"admin.ho_kinh_doanh.delete\",\"admin.ho_kinh_doanh.create\",\"admin.ho_kinh_doanh.show\",\"admin.ho_kinh_doanh.search\",\"admin.sap.list\",\"admin.sap.create\",\"admin.sap.store\",\"admin.sap.show\",\"admin.sap.edit\",\"admin.sap.update\",\"admin.sap.destroy\",\"admin.co_so_cung_cap.list\",\"admin.co_so_cung_cap.create\",\"admin.co_so_cung_cap.store\",\"admin.co_so_cung_cap.show\",\"admin.co_so_cung_cap.edit\",\"admin.co_so_cung_cap.update\",\"admin.co_so_cung_cap.destroy\",\"admin.nganh_kinh_doanh.list\",\"admin.nganh_kinh_doanh.create\",\"admin.nganh_kinh_doanh.store\",\"admin.nganh_kinh_doanh.show\",\"admin.nganh_kinh_doanh.edit\",\"admin.nganh_kinh_doanh.update\",\"admin.nganh_kinh_doanh.destroy\",\"admin.cho.list\",\"admin.cho.add\",\"admin.cho.edit\",\"admin.cho.update\",\"admin.cho.delete\",\"admin.thongke.mhkd.list\",\"admin.thongke.mhkd.export\",\"admin.thongke.mhkd.exportpdf\",\"admin.quanly.hoso.attp\",\"admin.quanly.hoso.attp.export\",\"admin.ho_kd.list\",\"admin.mat_hang_kinh_doanh.list\",\"admin.mat_hang_kinh_doanh.create\",\"admin.mat_hang_kinh_doanh.edit\",\"admin.mat_hang_kinh_doanh.delete\",\"admin.mat_hang_kinh_doanh.export\",\"admin.ATTP.list\",\"admin.don-vi-tinh.list\",\"admin.don-vi-tinh.create\",\"admin.don-vi-tinh.store\",\"admin.don-vi-tinh.show\",\"admin.don-vi-tinh.edit\",\"admin.don-vi-tinh.update\",\"admin.don-vi-tinh.destroy\"]',1,'2019-04-08 20:55:32','0000-00-00 00:00:00');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `alias` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` text COLLATE utf8_unicode_ci,
  `phone` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_group` int(10) unsigned DEFAULT NULL,
  `email` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `confirmation` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `permission` tinyint(3) unsigned NOT NULL DEFAULT '3',
  `id_skype` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`),
  KEY `USERS_GROUP` (`id_group`),
  CONSTRAINT `USERS_GROUP` FOREIGN KEY (`id_group`) REFERENCES `user_group` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`alias`,`avatar`,`phone`,`name`,`id_group`,`email`,`password`,`status`,`confirmation`,`permission`,`id_skype`,`remember_token`,`created_at`,`updated_at`,`deleted_at`) values 
(2,'',NULL,'13232132','admin',0,'admin@gmail.com','$2y$10$y12vzPdwPQp7/dw4r6PEZeAUTvS/DC6zFSzclbn7BnVKuEtNCrQRe','2',0,3,'',NULL,NULL,NULL,NULL);

/*Table structure for table `web_categories` */

DROP TABLE IF EXISTS `web_categories`;

CREATE TABLE `web_categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `alias` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `image` text COLLATE utf8_unicode_ci,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `web_categories` */

/*Table structure for table `web_comments` */

DROP TABLE IF EXISTS `web_comments`;

CREATE TABLE `web_comments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ip` text COLLATE utf8_unicode_ci,
  `product_id` bigint(20) unsigned NOT NULL,
  `user_name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `COMMENTS_PRODUCTS` (`product_id`),
  CONSTRAINT `COMMENTS_PRODUCTS` FOREIGN KEY (`product_id`) REFERENCES `web_products` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `web_comments` */

/*Table structure for table `web_icons` */

DROP TABLE IF EXISTS `web_icons`;

CREATE TABLE `web_icons` (
  `id` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `image` text COLLATE utf8_unicode_ci,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `web_icons` */

/*Table structure for table `web_logs` */

DROP TABLE IF EXISTS `web_logs`;

CREATE TABLE `web_logs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `level` enum('warning','error','info','normal') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'normal',
  `url` text COLLATE utf8_unicode_ci,
  `ip` text COLLATE utf8_unicode_ci,
  `data` text COLLATE utf8_unicode_ci,
  `message` text COLLATE utf8_unicode_ci,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `web_logs` */

/*Table structure for table `web_order_detail` */

DROP TABLE IF EXISTS `web_order_detail`;

CREATE TABLE `web_order_detail` (
  `order_id` bigint(20) unsigned NOT NULL,
  `product_id` bigint(20) unsigned NOT NULL,
  `capital_current` bigint(20) NOT NULL,
  `price_current` bigint(20) NOT NULL,
  `amount` bigint(20) NOT NULL,
  `amount_delivered` bigint(20) NOT NULL,
  `amount_cancel` bigint(20) NOT NULL,
  `status` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`order_id`,`product_id`),
  KEY `DETAIL_PRODUCTS` (`product_id`),
  CONSTRAINT `DETAIL_ORDERS` FOREIGN KEY (`order_id`) REFERENCES `web_orders` (`id`),
  CONSTRAINT `DETAIL_PRODUCTS` FOREIGN KEY (`product_id`) REFERENCES `web_products` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `web_order_detail` */

/*Table structure for table `web_orders` */

DROP TABLE IF EXISTS `web_orders`;

CREATE TABLE `web_orders` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ip` text COLLATE utf8_unicode_ci,
  `user_name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `status` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `ship_price` bigint(20) NOT NULL,
  `delivery_at` datetime DEFAULT NULL,
  `delivered_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `web_orders` */

/*Table structure for table `web_parent_categories` */

DROP TABLE IF EXISTS `web_parent_categories`;

CREATE TABLE `web_parent_categories` (
  `category_id` bigint(20) unsigned NOT NULL,
  `parent_id` bigint(20) unsigned NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`category_id`,`parent_id`),
  KEY `CATEGORIES_PARENT` (`parent_id`),
  CONSTRAINT `CATEGORIES_ID` FOREIGN KEY (`category_id`) REFERENCES `web_categories` (`id`),
  CONSTRAINT `CATEGORIES_PARENT` FOREIGN KEY (`parent_id`) REFERENCES `web_categories` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `web_parent_categories` */

/*Table structure for table `web_products` */

DROP TABLE IF EXISTS `web_products`;

CREATE TABLE `web_products` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `alias` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `label` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` text COLLATE utf8_unicode_ci,
  `link` text COLLATE utf8_unicode_ci,
  `views` bigint(20) unsigned NOT NULL DEFAULT '0',
  `description` text COLLATE utf8_unicode_ci,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `category_id` bigint(20) unsigned NOT NULL,
  `capital` bigint(20) NOT NULL,
  `price` bigint(20) unsigned NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`),
  KEY `PRODUCTS_CATEGORIES` (`category_id`),
  KEY `PRODUCTS_USERS` (`user_id`),
  CONSTRAINT `PRODUCTS_CATEGORIES` FOREIGN KEY (`category_id`) REFERENCES `web_categories` (`id`),
  CONSTRAINT `PRODUCTS_USERS` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `web_products` */

/*Table structure for table `web_products_tags` */

DROP TABLE IF EXISTS `web_products_tags`;

CREATE TABLE `web_products_tags` (
  `product_id` bigint(20) unsigned NOT NULL,
  `tag_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`product_id`,`tag_id`),
  KEY `TAGS_PRODUCTS` (`tag_id`),
  CONSTRAINT `PRODUCTS_TAGS` FOREIGN KEY (`product_id`) REFERENCES `web_products` (`id`),
  CONSTRAINT `TAGS_PRODUCTS` FOREIGN KEY (`tag_id`) REFERENCES `web_tags` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `web_products_tags` */

/*Table structure for table `web_tags` */

DROP TABLE IF EXISTS `web_tags`;

CREATE TABLE `web_tags` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `alias` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `web_tags` */

/*Table structure for table `web_views` */

DROP TABLE IF EXISTS `web_views`;

CREATE TABLE `web_views` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `products_id` bigint(150) unsigned NOT NULL,
  `ip` text COLLATE utf8_unicode_ci,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `VIEWS_PRODUCTS` (`products_id`),
  CONSTRAINT `VIEWS_PRODUCTS` FOREIGN KEY (`products_id`) REFERENCES `web_products` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `web_views` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
