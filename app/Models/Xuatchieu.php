<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

abstract class XuatChieuRE {
    const DANHSACHDATVEONLINE = 'danhsachdatveonline';
    const VE = 've';
    const LICHCHIEU = 'lichchieu';
}

class Xuatchieu extends Model
{
    //api_gallery
    use SoftDeletes;
    protected $table = 'tb_xuatchieu';
    protected $fillable = [
        'id',
        'giochieu'
    ];
    protected $dates = ['deleted_at'];
    
    public $timestamps = false;

    public function danhsachdatveonline()
    {
        return $this->hasMany('App\Models\Dsdatveonline','id','id_xuat_chieu');
    }
    public function ve()
    {
        return $this->hasMany('App\Models\Ve', 'id','id_xuat_chieu');
    }

    public function lichchieu()
    {
        return $this->belongsToMany('App\Models\Phong','tb_lichchieu', 'id_phong','id_xuat_chieu');
    }


    public function getXuatChieuQuery()
    {
        return $this->with(XuatChieuRE::DANHSACHDATVEONLINE)
                    ->with(XuatChieuRE::VE)
                    ->with(XuatChieuRE::LICHCHIEU);
    }
}
