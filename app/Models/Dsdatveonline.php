<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

abstract class DsdatveonlineRE {
    const USER = 'user';
    const KHUVUC = 'khuvuc';
    const RAP = 'rap';
    const PHIM = 'phim';
    const XUATCHIEU = 'xuatchieu';
    const GHE = 'ghe';
    const LICHCHIEU = 'lichchieu';

}

class Dsdatveonline extends Model
{
    //api_gallery
    use SoftDeletes;
    protected $table = 'tb_dsdatveonline';
    protected $fillable = [
        'id',
        'id_user',
        'id_khu_vuc',
        'id_rap',
        'ngay_chieu',
        'phim',
        'phong',
        'xuat_chieu',
        'so_ghe'
    ];
    protected $dates = ['deleted_at'];
    
    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo('App\User', 'id_user');
    }

    public function khuvuc()
    {
        return $this->belongsTo('App\Models\Khuvuc', 'id_khu_vuc');
    }

    public function rap()
    {
        return $this->belongsTo('App\Models\Rap', 'id_rap');
    }

    public function phim()
    {
        return $this->belongsTo('App\Models\Phim', 'id_phim');
    }

    public function xuatchieu()
    {
        return $this->belongsTo('App\Models\Xuatchieu', 'id_xuat_chieu');
    }

    public function ghe()
    {
        return $this->belongsTo('App\Models\Ghe', 'id_ghe');
    }

    public function lichchieu()
    {
        return $this->belongsTo('App\Models\Lichchieu', 'id_ngay_chieu');
    }

    public function getDsdatveonlineQuery()
    {
        return $this->with(DsdatveonlineRE::USER)
                    ->with(DsdatveonlineRE::XUATCHIEU)
                    ->with(DsdatveonlineRE::RAP)
                    ->with(DsdatveonlineRE::PHIM)
                    ->with(DsdatveonlineRE::GHE)
                    ->with(DsdatveonlineRE::KHUVUC)
                    ->with(DsdatveonlineRE::LICHCHIEU);
    }
}
