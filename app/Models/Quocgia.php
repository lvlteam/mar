<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

abstract class QuocGiaRE {
    const PHIM = 'phim';
}

class Quocgia extends Model
{
    //api_gallery
    use SoftDeletes;
    protected $table = 'tb_quocgia';
    protected $fillable = [
        'id',
        'tenquocgia',
        'la_co_quoc_gia',
    ];
    protected $dates = ['deleted_at'];
    
    public $timestamps = false;

    public function phim()
    {
        return $this->hasMany('App\Models\Phim', 'id_quoc_gia','id');
    }

    public function getQuocGiaQuery()
    {
        return $this->with(QuocGiaRE::PHIM);
    }
}
