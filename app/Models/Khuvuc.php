<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

abstract class KhuVucRE {
    const RAP = 'rap';
    const PHIM = 'phim';
    const DSDATVEONLINE = 'danhsachdatveonline';
}

class KhuVuc extends Model
{
    //api_gallery
    use SoftDeletes;
    protected $table = 'tb_khuvuc';
    protected $fillable = [
        'id',
        'tenkhuvuc'
    ];
    protected $dates = ['deleted_at'];
    
    public $timestamps = false;

    public function rap()
    {
        return $this->hasMany('App\Models\Rap', 'id_khu_vuc','id');
    }

    public function phim()
    {
        return $this->hasMany('App\Models\Phim', 'id_khu_vuc','id');
    
    }

    public function danhsachdatveonline()
    {
        return $this->hasMany('App\Models\Dsdatveonline','id','id_khu_vuc');
    }

    public function getKhuVucQuery()
    {
        return $this->with(KhuVucRE::RAP)
                    ->with(KhuVucRE::PHIM)
                    ->with(KhuVucRE::DSDATVEONLINE);
    }
}
