<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
// use Illuminate\Foundation\Auth\Thanhvien as Authenticatable;

abstract class ThanhVienRE {
    const DANHSACHDATVEONLINE = 'danhsachdatveonline';
    const BINHLUAN = 'binhluan';
}

class Thanhvien extends Model
{
    //api_gallery
    use SoftDeletes;
    protected $table = 'tb_thanhvien';
    protected $fillable = [
        'id',
        'hoten',
        'ngaysinh',
        'diachi',
        'password',
        'sodienthoai',
        'email',
        'gioitinh',
        'hinhanh'
    ];
    protected $dates = ['deleted_at'];
    
    public $timestamps = false;

    public function danhsachdatveonline()
    {
        return $this->hasMany('App\Models\Dsdatveonline', 'id','id_thanh_vien');
    }

    public function binhluan()
    {
        return $this->hasMany('App\Models\Binhluan', 'id','id_thanh_vien');
    }

    public function getThanhVienQuery()
    {
        return $this->with(ThanhVienRE::DANHSACHDATVEONLINE)
                    ->with(ThanhVienRE::BINHLUAN);
    }
}
