<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Chitietghe extends Model
{
    //api_gallery
    use SoftDeletes;
    protected $table = 'tb_chitietghe';
    protected $fillable = [
        'soghe',
        'mave',
    ];
    protected $dates = ['deleted_at'];
    
    public $timestamps = true;
}
