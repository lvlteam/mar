<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

abstract class RapRE {
    const PHIM = 'phim';
    const KHUVUC = 'khuvuc';
    const RAP = 'rap';
    const DSDATVEONLINE = 'danhsachdatveonline';  
}

class Rap extends Model
{
    //api_gallery
    use SoftDeletes;
    protected $table = 'tb_rap';
    protected $fillable = [
        'id',
        'tenrap',
        'id_khu_vuc'
    ];
    protected $dates = ['deleted_at'];
    
    public $timestamps = false;

    public function phim()
    {
        return $this->hasMany('App\Models\Phim', 'id_rap','id');
    }

    public function khuvuc()
    {
        return $this->belongsTo('App\Models\Khuvuc','id_khu_vuc');
    }

     public function lichchieu(){
        return $this->hasMany('App\Models\Lichchieu','id','id_rap');
    }

    public function danhsachdatveonline()
    {
        return $this->hasMany('App\Models\Dsdatveonline','id','id_rap');
    }


    public function getRapQuery()
    {
        return $this->with(RapRE::PHIM)
                    ->with(RapRE::KHUVUC)
                    ->with(RapRE::DSDATVEONLINE);    
    }
}
