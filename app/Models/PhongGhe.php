<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;

abstract class PhongGheRE {
    const PHONG = 'phong';
    const GHE = 'ghe';
    const LICHCHIEU = 'lichchieu';
}

class PhongGhe extends Model
{
    //api_gallery
    //use SoftDeletes;
    protected $table = 'tb_phong_ghe';
    //protected $primaryKey = 'id_phong';
    protected $fillable = [
        'id',
        'id_phong',
        'id_ghe'
    ];
    //protected $dates = ['deleted_at'];
    
    public $timestamps = false;

    public function phong()
    {
        return $this->belongsTo('App\Models\Phong', 'id_phong');
    }

    public function ghe()
    {
        return $this->belongsTo('App\Models\Ghe', 'id_ghe');
    }

    public function lichchieu(){
        return $this->hasMany('App\Models\PhongGhe','id','id_phong');
    }

    public function getPhongGheQuery()
    {
        return $this->with(PhongGheRE::PHONG)
                    ->with(PhongGheRE::GHE)
                    ->with(PhongGheRE::LICHCHIEU);
    }
}
