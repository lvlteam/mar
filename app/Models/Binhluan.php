<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

abstract class BinhluanRE {
    const THANHVIEN = 'thanhvien';
    const PHIM = 'phim';
}

class Binhluan extends Model
{
    //api_gallery
    use SoftDeletes;
    protected $table = 'tb_binhluan';
    protected $fillable = [
        'id',
        'id_thanh_vien',
        'maphim',
        'noidungbinhluan'
    ];
    protected $dates = ['deleted_at'];
    
    public $timestamps = true;

    public function thanhvien()
    {
        return $this->belongsTo('App\Models\Thanhvien', 'id_thanh_vien');
    }

    public function phim()
    {
        return $this->belongsTo('App\Models\Phim', 'madangphim');
    }

    public function getBinhluanQuery()
    {
        return $this->with(BinhluanRE::PHIM)
                    ->with(BinhluanRE::THANHVIEN);
    }
}
