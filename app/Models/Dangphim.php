<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

abstract class DangPhimRE {
    const GIA = 'gia';
    const PHIM = 'phim';
}

class Dangphim extends Model
{
    //api_gallery
    use SoftDeletes;
    protected $table = 'tb_dangphim';
    protected $fillable = [
        'id',
        'tendangphim',
    ];
    protected $dates = ['deleted_at'];
    
    public $timestamps = false;

    public function gia()
    {
        return $this->hasMany('App\Models\Gia', 'madangphim');
    }

    public function phim()
    {
        return $this->hasMany('App\Models\Phim', 'id','id_dang_phim');
    }

    public function getDangPhimQuery()
    {
        return $this->with(DangPhimRE::PHIM)
                    ->with(DangPhimRE::GIA);
    }
}
