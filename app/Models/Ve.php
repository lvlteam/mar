<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

abstract class VeRE {
    const USER = 'user';
    const GIA = 'gia';
    const CHITIETGHE = 'chitietghe';
    const PHONG = 'phong';
    const LICHCHIEU = 'lichchieu';
    const SUKIEN = 'sukien';
    const XUATCHIEU = 'xuatchieu';
}

class Ve extends Model
{
    //api_gallery
    use SoftDeletes;
    protected $table = 'tb_ve';
    protected $fillable = [
        'id',
        'id_xuat_chieu',
        'id_phong',
        'id_user',
        'id_gia',
        'id_su_kien',
        'id_don_gia',
        'id_ghe',
        'id_lich_chieu',
        'id_phim'
    ];
    protected $dates = ['deleted_at'];
    
    public $timestamps = true;

    public function user()
    {
        return $this->belongsTo('App\User', 'id_user');
    }

    public function gia()
    {
        return $this->belongsTo('App\Models\Gia', 'id_gia');
    }

    public function chitietghe()
    {
        return $this->belongsToMany('App\Models\Chitietghe', 'tb_chitietghe','soghe','mave');
    }

    public function phong()
    {
        return $this->belongsTo('App\Models\Phong', 'id_phong');
    }

    public function lichchieu()
    {
        return $this->belongsTo('App\Models\Lichchieu', 'id_lich_chieu');
    }

    public function sukien()
    {
        return $this->belongsTo('App\Models\Sukien', 'id_su_kien');
    }

    public function xuatchieu()
    {
        return $this->belongsTo('App\Models\Xuatchieu', 'id_xuat_chieu');
    }


    public function getVeQuery()
    {
        return $this->with(VeRE::USER)
                    ->with(VeRE::GIA)
                    ->with(VeRE::CHITIETGHE)
                    ->with(VeRE::PHONG)
                    ->with(VeRE::LICHCHIEU)
                    ->with(VeRE::SUKIEN)
                    ->with(VeRE::XUATCHIEU);
    }
}
