<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

abstract class Gia {
    const DANGPHIM = 'dangphim';
    const LOAIGHE   = 'loaighe';
    const VE = 've';
}

class Gia extends Model
{
    //api_gallery
    use SoftDeletes;
    protected $table = 'tb_gia';
    protected $fillable = [
        'magia',
        'maloaighe',
        'madangphim',
        'sotien'
    ];
    protected $dates = ['deleted_at'];
    
    public $timestamps = true;

    public function dangphim()
    {
        return $this->belongsTo('App\Models\Dangphim', 'madangphim');
    }

    public function loaighe()
    {
        return $this->belongsTo('App\Models\Loaighe', 'maloaighe');
    }

    public function ve()
    {
        return $this->hasMany('App\Models\Ve', 'magia');;
    }

    public function getGiaQuery()
    {
        return $this->with(Gia::PHIM)
                    ->with(Gia::LOAIGHE)
                    ->with(Gia::VE);
    }
}
