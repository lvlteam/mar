<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

abstract class SukienRE{
    const VE = 've';
}

class Sukien extends Model
{
    //api_gallery
    use SoftDeletes;
    protected $table = 'tb_sukien';
    protected $fillable = [
        'id',
        'tensukien',
        'noidung',
        'ngaybatdau',
        'ngayketthuc',
        'avarta_sukien',
        'mucgiamgia',
        'ghichu'
    ];
    protected $dates = ['deleted_at'];
    
    public $timestamps = false;

    public function ve()
    {
        return $this->hasMany('App\Models\Ve', 'id','id_su_kien');
    }

    public function getSukienQuery()
    {
        return $this->with(SukienRE::VE);
    }
}
