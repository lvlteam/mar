<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

abstract class PhimRE {
    const BINHLUAN = 'binhluan';
    const LOAIPHIM = 'loaiphim';
    const DANGPHIM = 'dangphim';
    const QUOCGIA = 'quocgia';
    const RAP = 'rap';
    const KHUVUC = 'khuvuc';
    const LICHCHIEU = 'lichchieu';
    const DSDATVEONLINE = 'danhsachdatveonline';
    const NEWS = 'news';
}

class Phim extends Model
{
    //api_gallery
    use SoftDeletes;
    protected $table = 'tb_phim';
    protected $fillable = [
        'id',
        'tenphim',
        'id_loai_phim',
        'dodaigio',
        'dodaiphut',
        'id_quoc_gia',
        'ngaybatdau',
        'ngayketthuc',
        'avarta',
        'ghichu',
        'motaphim',
        'id_dang_phim',
        'video',
        'tomtat',
        'year',
        'new',
        'status'
    ];
    protected $dates = ['deleted_at'];
    
    public $timestamps = false;

    public function binhluan()
    {
        return $this->hasMany('App\Models\Binhluan', 'maphim');
    }

    public function loaiphim()
    {
        return $this->belongsTo('App\Models\Loaiphim', 'id_loai_phim');
    }

    public function dangphim()
    {
        return $this->belongsTo('App\Models\Dangphim', 'id_dang_phim');
    }

    public function quocgia()
    {
        return $this->belongsTo('App\Models\Quocgia', 'id_quoc_gia');
    }

    public function rap()
    {
        return $this->belongsTo('App\Models\Rap','id_rap');
    }
    public function khuvuc()
    {
        return $this->belongsTo('App\Models\Khuvuc','id_khu_vuc');
    }

    public function lichchieu(){
        return $this->hasMany('App\Models\Lichchieu','id','id_phim');
    }

    public function danhsachdatveonline()
    {
        return $this->hasMany('App\Models\Dsdatveonline','id','id_phim');
    }

    public function news()
    {
        return $this->belongsTo('App\Models\News','id','id_phim');
    }

    public function getPhimQuery()
    {
        return $this->with(PhimRE::BINHLUAN)
                    ->with(PhimRE::LOAIPHIM)
                    ->with(PhimRE::DANGPHIM)
                    ->with(PhimRE::QUOCGIA)
                    ->with(PhimRE::RAP)
                    ->with(PhimRE::KHUVUC)
                    ->with(PhimRE::LICHCHIEU)
                    ->with(PhimRE::DSDATVEONLINE)
                    ->with(PhimRE::NEWS);
    }
}
