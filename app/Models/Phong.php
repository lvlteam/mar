<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

abstract class PhongRE {
    const VE = 've';
    const DANGPHONG = 'dangphong';
    const LICHCHIEU = 'lichchieu';
    const PHONGGHE = 'phongghe';
}

class Phong extends Model
{
    //api_gallery
    use SoftDeletes;
    protected $table = 'tb_phong';
    protected $fillable = [
        'id',
        'tenphong',
        'id_dang_phong',
        'trangthaiphong',
        'SLghedaytoida',
        'Slghemoiday',
        'ghichu'
    ];
    protected $dates = ['deleted_at'];
    
    public $timestamps = false;

    // public function ghe()
    // {
    //     return $this->hasMany('App\Models\Ghe', 'id_phong');
    // }

    public function phongghe()
    {
        return $this->belongsToMany('App\Models\Ghe','tb_phong_ghe', 'id_phong','id_ghe');
    }
    public function ve()
    {
        return $this->hasMany('App\Models\Ve', 'id','id_phong');
    }

    public function dangphong()
    {
        return $this->belongsTo('App\Models\Dangphong','id_dang_phong');
    }

    public function lichchieu()
    {
        return $this->belongsToMany('App\Models\Xuatchieu','tb_lichchieu', 'id_xuat_chieu','id_phong');
    }

    public function getPhongQuery()
    {
        return $this->with(PhongRE::VE)
                    ->with(PhongRE::DANGPHONG)
                    ->with(PhongRE::LICHCHIEU)
                    ->with(PhongRE::PHONGGHE);
                    
    }
}
