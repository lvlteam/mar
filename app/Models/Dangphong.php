<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

abstract class DangPhongRE {
    const PHONG = 'phong';
}

class Dangphong extends Model
{
    //api_gallery
    use SoftDeletes;
    protected $table = 'tb_dangphong';
    protected $fillable = [
        'id',
        'tendangphong'
    ];
    protected $dates = ['deleted_at'];
    
    public $timestamps = true;

    public function phong()
    {
        return $this->hasMany('App\Models\Phong', 'id','id_dang_phong');
    }
    public function getDangPhongQuery()
    {
        return $this->with(DangPhongRE::PHONG);
    }
}
