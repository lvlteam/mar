<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

abstract class LoaiPhimRE {
    const PHIM = 'phim';
}

class Loaiphim extends Model
{
    //api_gallery
    use SoftDeletes;
    protected $table = 'tb_loaiphim';
    protected $fillable = [
        'id',
        'tenloaiphim',
    ];
    protected $dates = ['deleted_at'];
    
    public $timestamps = false;

    public function phim()
    {
        return $this->hasMany('App\Models\Phim', 'id','id_loai_phim');
    }

    public function getLoaiPhimQuery()
    {
        return $this->with(LoaiPhimRE::PHIM);
    }
}
