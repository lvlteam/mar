<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

abstract class LoaiGheRE {
    const GIA = 'gia';
    const GHE   = 'ghe';
}

class Loaighe extends Model
{
    //api_gallery
    use SoftDeletes;
    protected $table = 'tb_loaighe';
    protected $fillable = [
        'id',
        'tenloaighe'
    ];
    protected $dates = ['deleted_at'];
    
    public $timestamps = true;

    public function gia()
    {
        return $this->hasMany('App\Models\Gia', 'id');
    }

    public function ghe()
    {
        return $this->hasMany('App\Models\Ghe', 'id','id_ghe');
    }
    public function getLoaiGheQuery()
    {
        return $this->with(LoaiGheRE::GIA)
                    ->with(LoaiGheRE::GHE);
    }
}
