<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

abstract class NewsRE {
    const PHIM = 'phim';
}

class News extends Model
{
    //api_gallery
    // use SoftDeletes;
    protected $table = 'tb_news';
    protected $fillable = [
        'id',
        'id_phim',
        'tieu_de',
        'noi_dung',
        'noi_dung_chinh',
        'ngay_dang',
        'luot_xem',
        'status'

    ];
    // protected $dates = ['deleted_at'];
    
    public $timestamps = false;

    public function phim()
    {
        return $this->belongsTo('App\Models\Phim', 'id_phim');
    }

    public function getNewQuery()
    {
        return $this->with(NewsRE::PHIM);
    }
}
