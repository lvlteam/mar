<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

abstract class GheRE {
    const LOAIGHE = 'loaighe';
    const TINHTRANG = 'tinhtrang';
    const CHITIETGHE = 'chitietghe';
    const PHONG = 'phong';
    const LICHCHIEU = 'lichchieu';
    const DSDATVEONLINE = 'danhsachdatveonline';
}

class Ghe extends Model
{
    //api_gallery
    use SoftDeletes;
    protected $table = 'tb_ghe';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id',
        'id_phong',
        'tenghe',
        'matinhtrang',
        'trangthaighe'
    ];
    protected $dates = ['deleted_at'];
    
    public $timestamps = true;

    public function loaighe()
    {
        return $this->belongsTo('App\Models\Loaighe', 'id_loai_ghe');
    }

    public function tinhtrang()
    {
        return $this->belongsTo('App\Models\TinhTrang', 'matinhtrang');
    }

    public function chitietghe()
    {
        return $this->belongsToMany('App\Models\Ve', 'tb_chitietghe','mave','soghe');
    }

    // public function phong()
    // {
    //     return $this->belongsTo('App\Models\Phong', 'id_phong');
    // }

    public function lichchieu(){
        return $this->hasMany('App\Models\Lichchieu','id','id_phong');
    }

    public function phongghe()
    {
        return $this->belongsToMany('App\Models\Phong','tb_phong_ghe','id_phong','id_ghe');
    }

    public function danhsachdatveonline()
    {
        return $this->hasMany('App\Models\Dsdatveonline','id','so_ghe');
    }

    public function getGheQuery()
    {
        return $this->with(GheRE::LOAIGHE)
                    ->with(GheRE::TINHTRANG)
                    ->with(GheRE::CHITIETGHE)
                    ->with(GheRE::PHONG)
                    ->with(GheRE::LICHCHIEU)
                    ->with(GheRE::DSDATVEONLINE);
    }
}
