<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;

abstract class LichChieuRE {
    const XUATCHIEU = 'xuatchieu';
    const VE = 've';
    const PHONG = 'phong';
    const PHIM = 'phim';
    const RAP = 'rap';
    const GHE = 'ghe';
    const PHONGGHE = 'phongghe';
    const DSDATVEONLINE ='dsdatveonline';
}

class Lichchieu extends Model
{
    //api_gallery
    //use SoftDeletes;
    protected $table = 'tb_lichchieu';
    // protected $primaryKey = 'id_phong';
    protected $fillable = [
        'id',
        'id_phong',
        'id_xuat_chieu',
        'ngaychieu',
        'id_phim',
        'id_rap',
    ];
    //protected $dates = ['deleted_at'];
    
    public $timestamps = false;

    public function xuatchieu()
    {
        return $this->belongsTo('App\Models\Xuatchieu', 'id_xuat_chieu');
    }

    public function ve()
    {
        return $this->hasMany('App\Models\Ve', 'id','id_lich_chieu');
    }

    public function phong()
    {
        return $this->belongsTo('App\Models\Phong', 'id_phong');
    }

    public function phim(){
        return $this->belongsTo('App\Models\Phim','id_phim');
    }

    public function rap(){
        return $this->belongsTo('App\Models\Rap','id_rap');
    }

    public function ghe()
    {
        return $this->belongsTo('App\Models\Ghe', 'id_phong');
    }

    public function phongghe(){
        return $this->belongsTo('App\Models\PhongGhe','id_phong');
    }

    public function dsdatveonline()
    {
        return $this->hasMany('App\Models\Dsdatveonline', 'id','id_ngay_chieu');
    }

    public function getlichchieuQuery()
    {
        return $this->with(LichChieuRE::XUATCHIEU)
                    ->with(LichChieuRE::VE)
                    ->with(LichChieuRE::PHONG)
                    ->with(LichChieuRE::PHIM)
                    ->with(LichChieuRE::RAP)
                    ->with(LichChieuRE::GHE)
                    ->with(LichChieuRE::PHONGGHE)
                    ->with("phong.phongghe")
                    ->with(LichChieuRE::DSDATVEONLINE);
    }
}
