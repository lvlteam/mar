<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

abstract class Tinhtrang {
    const GHE = 'ghe';
}

class TinhTrang extends Model
{
    //api_gallery
    use SoftDeletes;
    protected $table = 'tb_tinhtrang';
    protected $fillable = [
        'matinhtrang',
        'tinhtrangghe'
    ];
    protected $dates = ['deleted_at'];
    
    public $timestamps = true;

    public function ghe()
    {
        return $this->hasMany('App\Models\Ghe', 'matinhtrang');
    }

    public function getTinhTrangQuery()
    {
        return $this->with(Tinhtrang::GHE);
    }
}
