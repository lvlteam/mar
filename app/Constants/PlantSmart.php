<?php
namespace App\Constants;
final class PlantSmart
{
    const HIRE = 0;
    const DOCUMENT_STATUS = 1;
    const DOWNLOAD = 2;
    const DEFECTS_STATUS = 3;
    const INSPECTION_STATUS = 4;
    const RESOURCE_FINDING_STATUS = 5;
    const RISK_ASSESSMENT_STATUS = 6;
    const RESOURCE_DASHBOARD_TYPE = 7;
    const RESOURCE_DASHBOARD_STATUS = 8;
    const USER_STATUS = 9;
    const MANAGE_RESOURCE = 10;
    const REGISTRATION_STATUS = 11;
}
?>