<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\Contracts\LoaiPhimRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Models\loaiphim;
use File;
use App\Constants\StatusCode;
use App\Constants\Message;

class LoaiPhimController extends Controller
{
    //
    private $laoiphim;


    public function __construct(LoaiPhimRepositoryInterface $loaiPhimRepository)
    {
        $this->loaiphim = $loaiPhimRepository;
    }

    public function index(Request $request)
    {
        
       


        $list['data'] = $this->loaiphim->listLoaiPhim();
         return view('admin.loai-phim.list',$list);
    }

    public function create()
    {
        return view('admin.loai-phim.add');
    }

    public function store(Request $request)
    {
         try {
            $validator = \Validator::make($request->all(), [
                'tenloaiphim' => 'required'
            ],[
                'tenloaiphim.required'=>'bạn chưa nhập tên loại phim'
            ]);

            if ($validator->fails()) {

                $data_errors = $validator->errors();

                $array = [];

                foreach ($data_errors->messages() as $key => $error) {

                    $array[] = ['key' => $key, 'mess' => $error];
                }

                return $this->dataError(Message::ERROR, $array, StatusCode::NOT_FOUND);

            } else {

                $dataLoaiPhim = $request->all();;

                $saveLoaiPhim = $this->loaiphim->save($dataLoaiPhim);

                if($saveLoaiPhim){

                    return redirect()->to(route('admin.loai-phim.list'))->with('thongbao','Bạn đã thêm thành công');
                }
            }

        } catch (Exception $e) {
            return $this->dataError(Message::SERVER_ERROR, null, StatusCode::SERVER_ERROR);
        }
    }

    public function show($id)
    {
        $view['data'] = $this->loaiphim->get($id);
        return view('admin.loai-phim.show',$view);
    }

    public function edit($id)
    {
        $view['data'] = $this->loaiphim->get($id);
        return view('admin.loai-phim.edit', $view);
    }

    public function update(Request $request, $id)
    {

        try {
            $validator = \Validator::make($request->all(), 
                [
                'tenloaiphim' => 'required'
                ],
                [
                'name.unique'=>'Loại phim chưa được sửa'
                ]);

            if ($validator->fails()) {

                $data_errors = $validator->errors();

                $array = [];

                foreach ($data_errors->messages() as $key => $error) {

                    $array[] = ['key' => $key, 'mess' => $error];
                }

                return $this->dataError(Message::ERROR, $array, StatusCode::NOT_FOUND);

            } else {

                //$dataMachineType = $request->all();
                //dd($dataMachineType);

                $dataLoaiPhim['tenloaiphim'] = $request->tenloaiphim;

                $saveLoaiPhim = $this->loaiphim->update($dataLoaiPhim,$id);

                if($saveLoaiPhim){

                    return redirect()->to(route('admin.loai-phim.list'))->with('thongbao','Bạn đã sửa thành công');
                }
            }

        } catch (Exception $e) {
            return $this->dataError(Message::SERVER_ERROR, null, StatusCode::SERVER_ERROR);
        }
    }

    public function delete($id)
    {        $result = $this->loaiphim->delete($id);
        if ($result) {
            return redirect()->to(route('admin.loai-phim.list'));
        } else {
            return redirect()->back()->withErrors('xóa không thành công');
        }

    }
}   
