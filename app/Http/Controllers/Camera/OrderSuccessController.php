<?php

namespace App\Http\Controllers\Camera;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Hash;
use Auth;
use App\User;
use App\Constants\StatusCode;
use App\Constants\Message;
use DB;

class OrderSuccessController extends Controller
{

    // public function __construct(UserRepositoryInterface $user,
    //     KhuVucRepositoryInterface $KhuVucRepository,
    //     RapRepositoryInterface $rapRepository,
    //     LichChieuRepositoryInterface $lichChieuRepository,
    //     XuatChieuRepositoryInterface $xuatChieuRepository,
    //     PhongRepositoryInterface $phongRepository,
    //     PhimRepositoryInterface $phimRepository,
    //     DsdatveonlineRepositoryInterface $dsdatveonlineRepository,
    //     LoaiPhimRepositoryInterface $loaiPhimRepository,
    //     QuocGiaRepositoryInterface $quocGiaRepository,
    //     NewsRepositoryInterface $newsRepository)
    // {
    //     $this->user = $user;
    //     $this->khuvuc = $KhuVucRepository;
    //     $this->rap = $rapRepository;
    //     $this->lichchieu = $lichChieuRepository;
    //     $this->phong = $phongRepository;
    //     $this->xuatchieu = $xuatChieuRepository;
    //     $this->phim = $phimRepository;
    //     $this->dsdatveonline = $dsdatveonlineRepository;
    //     $this->loaiphim = $loaiPhimRepository;
    //     $this->quocgia = $quocGiaRepository;
    //     $this->news = $newsRepository;

    // }

    public function index(Request $request){

        return view('Camera.ordersuccess.ordersuccess');
    }
}
