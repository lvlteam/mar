<?php

namespace App\Http\Controllers\Camera;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Hash;
use Auth;
use App\User;
use App\Constants\StatusCode;
use App\Constants\Message;
use DB;

class ListOfProductController extends Controller
{
    public function index(Request $request){

        return view('Camera.list_of_product.list_of_product');
    }
}
