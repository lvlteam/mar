<?php 
namespace App\Repositories; 
use Illuminate\Support\ServiceProvider; 
class RegisterRepositories extends ServiceProvider 
{ 
  public function register() 
  { 
  
        $this->app->bind( 
           'App\Repositories\Contracts\DonViTinhRepositoryInterface', 
           'App\Repositories\Eloquent\DonViTinhRepository' 
 
        );

         $this->app->bind( 
           'App\Repositories\Contracts\LoaiPhimRepositoryInterface', 
           'App\Repositories\Eloquent\LoaiPhimRepository' 
 
        );

         $this->app->bind( 
           'App\Repositories\Contracts\QuocGiaRepositoryInterface', 
           'App\Repositories\Eloquent\QuocGiaRepository' 
 
        );

         $this->app->bind( 
           'App\Repositories\Contracts\PhimRepositoryInterface', 
           'App\Repositories\Eloquent\PhimRepository' 
 
        );

         $this->app->bind( 
           'App\Repositories\Contracts\DangPhimRepositoryInterface', 
           'App\Repositories\Eloquent\DangPhimRepository' 
 
        );

         $this->app->bind( 
           'App\Repositories\Contracts\LichChieuRepositoryInterface', 
           'App\Repositories\Eloquent\LichChieuRepository' 
 
        );

         $this->app->bind( 
           'App\Repositories\Contracts\XuatChieuRepositoryInterface', 
           'App\Repositories\Eloquent\XuatChieuRepository' 
 
        );

         $this->app->bind( 
           'App\Repositories\Contracts\PhongRepositoryInterface', 
           'App\Repositories\Eloquent\PhongRepository' 
 
        );
        
        $this->app->bind( 
           'App\Repositories\Contracts\DangPhongRepositoryInterface', 
           'App\Repositories\Eloquent\DangPhongRepository' 
 
        );

        $this->app->bind( 
           'App\Repositories\Contracts\SuKienRepositoryInterface', 
           'App\Repositories\Eloquent\SuKienRepository' 
 
        );

        $this->app->bind( 
           'App\Repositories\Contracts\ThanhVienRepositoryInterface', 
           'App\Repositories\Eloquent\ThanhVienRepository' 
 
        );

        $this->app->bind( 
           'App\Repositories\Contracts\KhuVucRepositoryInterface', 
           'App\Repositories\Eloquent\KhuVucRepository' 
 
        );

        $this->app->bind( 
           'App\Repositories\Contracts\RapRepositoryInterface', 
           'App\Repositories\Eloquent\RapRepository' 
 
        );
        $this->app->bind( 
           'App\Repositories\Contracts\DsdatveonlineRepositoryInterface', 
           'App\Repositories\Eloquent\DsdatveonlineRepository' 
 
        );
        $this->app->bind( 
           'App\Repositories\Contracts\NewsRepositoryInterface', 
           'App\Repositories\Eloquent\NewsRepository' 
 
        );
      $this->app->bind(
          'App\Repositories\Contracts\UserRepositoryInterface',
          'App\Repositories\Eloquent\UserRepository'

      );
      $this->app->bind(
          'App\Repositories\Contracts\UserGroupRepositoryInterface',
          'App\Repositories\Eloquent\UserGroupRepository'

      );
         

  } 
} 
