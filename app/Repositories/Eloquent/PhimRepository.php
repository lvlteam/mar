<?php
namespace App\Repositories\Eloquent;

use App\Models\Loaiphim;
use App\Models\Phim;
use App\Models\Quocgia;
use App\Repositories\Contracts\PhimRepositoryInterface;


class PhimRepository implements PhimRepositoryInterface
{
	private $loaiphim;
    private $phim;
    private $quocgia;
	public function __construct() { $this->phim = new Phim();}


	public function get($id,$columns = array('*'))
        {
                    $data = $this->phim->find($id, $columns);
                        if ($data)
                        {
                            return $data;
                        }
                        return null;

        }
	public function all($columns = array('*'))
        {
            $listData = $this->phim->get($columns);
            return $listData;
        }
	public function paginate($perPage = 15,$columns = array('*'))
        {
            $listData = $this->phim->paginate($perPage, $columns);
            return $listData;
        }
	public function save(array $data)
        {
        return $this->phim->create($data);

        }
	public function update(array $data,$id) {
         $dep =  $this->phim->find($id);
        if ($dep)
        {
            foreach ($dep->getFillable() as $field)
            {
                if (array_key_exists($field,$data)){
                    $dep->$field = $data[$field];
                }
            }
            if ($dep->save())
            {
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
        }
	public function getByColumn($column,$value,$columnsSelected = array('*'))
        {

             $data = $this->phim->where($column,$value)->first();
            if ($data)
            {
                return $data;
            }
            return null;


        }
        public function getByColumn1($column,$value,$columnsSelected = array('*'))
        {       
            $data = $this->phim->getPhimQuery()->where($column,$value)->paginate(4);

            if ($data)
            {
               
                return $data;
            }
            return null;


        }
	public function getByMultiColumn(array $where,$columnsSelected = array('*'))
        {

             $data = $this->phim;

            foreach ($where as $key => $value) {
                $data = $data->where($key, $value);
            }

            $data = $data->first();


            if ($data)
            {
                return $data;
            }
            return null;


        }
	public function getListByColumn($column,$value,$columnsSelected = array('*'))
        {

             $data = $this->phim->where($column,$value)->get();
            if ($data)
            {
                return $data;
            }
            return null;


        }
	public function getListByMultiColumn(array $where,$columnsSelected = array('*'))
        {

             $data = $this->phim;

              foreach ($where as $key => $value) {
            $data = $data->where($key, $value);
        }

        $data = $data->get();

            if ($data)
            {
                return $data;
            }
            return null;


        }
	public function delete($id)
        {
            $del = $this->phim->find($id);
            if ($del !== null)
            {
                $del->delete();
                return true;
            }
            else{
                return false;
            }
        }

	public function deleteMulti(array $data)
        {
            $del = $this->loaiphim->whereIn("id",$data["list_id"])->delete();
            if ($del)
            {

                return true;
            }
            else{
                return false;
            }
        }
        
    public function listPhim($query){
        $phim = $this->phim->getPhimQuery();

        $phim = $this->searchPhim($phim,$query);

        $phim = $this->fillterPhim($phim,$query)->get();
        //dd($phim);
        return $phim;
        
        }
    public function Phim2015($query){
        $phim = $this->phim->getPhimQuery();

        $phim = $this->phim->where('year','=','2015');

         $phim = $this->searchPhim($phim,$query);

        return $phim->get();
    }
    public function Phim2016($query){
        $phim = $this->phim->getPhimQuery();

        $phim = $this->phim->where('year','=','2016');

         $phim = $this->searchPhim($phim,$query);

        return $phim->get();
    }
    public function Phim2017($query){
        $phim = $this->phim->getPhimQuery();

        $phim = $this->phim->where('year','=','2017');

         $phim = $this->searchPhim($phim,$query);

        return $phim->get();
    } 
    public function Phim2018($query){
        $phim = $this->phim->getPhimQuery();

        $phim = $this->phim->where('year','=','2018');

         $phim = $this->searchPhim($phim,$query);

        return $phim->get();
    } 
    public function newPhim($query){

        $phim = $this->phim->getPhimQuery();

        $phim = $this->phim->where('year','=','2018')->where('new','=','1')->take(10)->orderBy('id','desc');

        $phim = $this->searchPhim($phim,$query);

        return $phim->get();
    }

    public function fillterPhim($list,$query)
    {
        if ($query != null) {
            if (isset($query['id_loai_phim']) && $query['id_loai_phim'] != '') {

                $list = $list->whereHas('loaiphim', function ($list) use ($query) {

                    $list->where('id', $query['id_loai_phim']);
                });
            }
            
            if (isset($query['id_dang_phim']) && $query['id_dang_phim'] != '') {

                $list = $list->whereHas('dangphim', function ($list) use ($query) {

                    $list->where('id', $query['id_dang_phim']);
                });
            }

            if (isset($query['id_quoc_gia']) && $query['id_quoc_gia'] != '') {

                $list = $list->whereHas('quocgia', function ($list) use ($query) {

                    $list->where('id', $query['id_quoc_gia']);
                });
            }

            // if (isset($query['id_khu_vuc']) && $query['id_khu_vuc'] != '') {

            //     $list = $list->whereHas('khuvuc', function ($list) use ($query) {

            //         $list->where('id', $query['id_khu_vuc']);
            //     });
            // }
        }
        return $list;
    }

    public function searchPhim($list,$query)
    {
        if($query != null){
            if(isset($query['tenphim']) && $query['tenphim'] != ''){
                $list = $list->where('tenphim','like','%'.$query['tenphim'].'%');
            }
        }
        return $list;
    }    
          

}
