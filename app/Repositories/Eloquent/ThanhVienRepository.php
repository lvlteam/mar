<?php
namespace App\Repositories\Eloquent;

use App\Models\Thanhvien;
use App\Repositories\Contracts\ThanhVienRepositoryInterface;

class ThanhVienRepository implements ThanhVienRepositoryInterface
{
	private $thanhvien;
	public function __construct() { $this->thanhvien = new Thanhvien();}


	public function get($id,$columns = array('*'))
        {
                    $data = $this->thanhvien->find($id, $columns);
                        if ($data)
                        {
                            return $data;
                        }
                        return null;

        }
	public function all($columns = array('*'))
        {
            $listData = $this->thanhvien->get($columns);
            return $listData;
        }
	public function paginate($perPage = 15,$columns = array('*'))
        {
            $listData = $this->thanhvien->paginate($perPage, $columns);
            return $listData;
        }
	public function save(array $data)
        {
        return $this->thanhvien->create($data);

        }
	public function update(array $data,$id) {
         $dep =  $this->thanhvien->find($id);
        if ($dep)
        {
            foreach ($dep->getFillable() as $field)
            {
                if (array_key_exists($field,$data)){
                    $dep->$field = $data[$field];
                }
            }
            if ($dep->save())
            {
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
        }
	public function getByColumn($column,$value,$columnsSelected = array('*'))
        {

             $data = $this->thanhvien->where($column,$value)->first();
            if ($data)
            {
                return $data;
            }
            return null;


        }
	public function getByMultiColumn(array $where,$columnsSelected = array('*'))
        {

             $data = $this->thanhvien;

            foreach ($where as $key => $value) {
                $data = $data->where($key, $value);
            }

            $data = $data->first();


            if ($data)
            {
                return $data;
            }
            return null;


        }
	public function getListByColumn($column,$value,$columnsSelected = array('*'))
        {

             $data = $this->thanhvien->where($column,$value)->get();
            if ($data)
            {
                return $data;
            }
            return null;


        }
	public function getListByMultiColumn(array $where,$columnsSelected = array('*'))
        {

             $data = $this->thanhvien;

              foreach ($where as $key => $value) {
            $data = $data->where($key, $value);
        }

        $data = $data->get();

            if ($data)
            {
                return $data;
            }
            return null;


        }
	public function delete($id)
        {
            $del = $this->thanhvien->find($id);
            if ($del !== null)
            {
                $del->delete();
                return true;
            }
            else{
                return false;
            }
        }

	public function deleteMulti(array $data)
        {
            $del = $this->thanhvien->whereIn("id",$data["list_id"])->delete();
            if ($del)
            {

                return true;
            }
            else{
                return false;
            }
        }
        
    public function listThanhVien($perPage = 15, $currentPage = null,$query = null){
        $thanhvien = $this->thanhvien->getThanhVienQuery();
        return $thanhvien->get();
        }  

}
