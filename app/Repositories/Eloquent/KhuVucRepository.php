<?php
namespace App\Repositories\Eloquent;

use App\Models\Khuvuc;;
use App\Repositories\Contracts\KhuVucRepositoryInterface;

class KhuVucRepository implements KhuVucRepositoryInterface
{
	private $khuvuc;
	public function __construct() { $this->khuvuc = new Khuvuc();}


	public function get($id,$columns = array('*'))
        {
                    $data = $this->khuvuc->find($id, $columns);
                        if ($data)
                        {
                            return $data;
                        }
                        return null;

        }
	public function all($columns = array('*'))
        {
            $listData = $this->khuvuc->get($columns);
            return $listData;
        }
	public function paginate($perPage = 15,$columns = array('*'))
        {
            $listData = $this->khuvuc->paginate($perPage, $columns);
            return $listData;
        }
	public function save(array $data)
        {
        return $this->khuvuc->create($data);

        }
	public function update(array $data,$id) {
         $dep =  $this->khuvuc->find($id);
        if ($dep)
        {
            foreach ($dep->getFillable() as $field)
            {
                if (array_key_exists($field,$data)){
                    $dep->$field = $data[$field];
                }
            }
            if ($dep->save())
            {
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
        }
	public function getByColumn($column,$value,$columnsSelected = array('*'))
        {

             $data = $this->khuvuc->where($column,$value)->first();
            if ($data)
            {
                return $data;
            }
            return null;


        }
	public function getByMultiColumn(array $where,$columnsSelected = array('*'))
        {

             $data = $this->khuvuc;

            foreach ($where as $key => $value) {
                $data = $data->where($key, $value);
            }

            $data = $data->first();


            if ($data)
            {
                return $data;
            }
            return null;


        }
	public function getListByColumn($column,$value,$columnsSelected = array('*'))
        {

             $data = $this->khuvuc->where($column,$value)->get();
            if ($data)
            {
                return $data;
            }
            return null;


        }
	public function getListByMultiColumn(array $where,$columnsSelected = array('*'))
        {

             $data = $this->khuvuc;

              foreach ($where as $key => $value) {
            $data = $data->where($key, $value);
        }

        $data = $data->get();

            if ($data)
            {
                return $data;
            }
            return null;


        }
	public function delete($id)
        {
            $del = $this->khuvuc->find($id);
            if ($del !== null)
            {
                $del->delete();
                return true;
            }
            else{
                return false;
            }
        }

	public function deleteMulti(array $data)
        {
            $del = $this->khuvuc->whereIn("id",$data["list_id"])->delete();
            if ($del)
            {

                return true;
            }
            else{
                return false;
            }
        }
        
    // public function listkhuvuc($perPage = 15, $currentPage = null,$query = null){
    //     $khuvuc = $this->khuvuc->getkhuvucQuery();

    //     return $khuvuc->get();
    //     }  

}
