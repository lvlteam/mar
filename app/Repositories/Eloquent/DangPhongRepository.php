<?php
namespace App\Repositories\Eloquent;

use App\Models\Dangphong;
use App\Repositories\Contracts\DangPhongRepositoryInterface;

class DangPhongRepository implements DangPhongRepositoryInterface
{
	private $dangphong;
	public function __construct() { $this->dangphong = new Dangphong();}


	public function get($id,$columns = array('*'))
        {
                    $data = $this->dangphong->find($id, $columns);
                        if ($data)
                        {
                            return $data;
                        }
                        return null;

        }
	public function all($columns = array('*'))
        {
            $listData = $this->dangphong->get($columns);
            return $listData;
        }
	public function paginate($perPage = 15,$columns = array('*'))
        {
            $listData = $this->dangphong->paginate($perPage, $columns);
            return $listData;
        }
	public function save(array $data)
        {
        return $this->dangphong->create($data);

        }
	public function update(array $data,$id) {
         $dep =  $this->dangphong->find($id);
        if ($dep)
        {
            foreach ($dep->getFillable() as $field)
            {
                if (array_key_exists($field,$data)){
                    $dep->$field = $data[$field];
                }
            }
            if ($dep->save())
            {
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
        }
	public function getByColumn($column,$value,$columnsSelected = array('*'))
        {

             $data = $this->dangphong->where($column,$value)->first();
            if ($data)
            {
                return $data;
            }
            return null;


        }
	public function getByMultiColumn(array $where,$columnsSelected = array('*'))
        {

             $data = $this->dangphong;

            foreach ($where as $key => $value) {
                $data = $data->where($key, $value);
            }

            $data = $data->first();


            if ($data)
            {
                return $data;
            }
            return null;


        }
	public function getListByColumn($column,$value,$columnsSelected = array('*'))
        {

             $data = $this->dangphong->where($column,$value)->get();
            if ($data)
            {
                return $data;
            }
            return null;


        }
	public function getListByMultiColumn(array $where,$columnsSelected = array('*'))
        {

             $data = $this->dangphong;

              foreach ($where as $key => $value) {
            $data = $data->where($key, $value);
        }

        $data = $data->get();

            if ($data)
            {
                return $data;
            }
            return null;


        }
	public function delete($id)
        {
            $del = $this->dangphong->find($id);
            if ($del !== null)
            {
                $del->delete();
                return true;
            }
            else{
                return false;
            }
        }

	public function deleteMulti(array $data)
        {
            $del = $this->dangphong->whereIn("id",$data["list_id"])->delete();
            if ($del)
            {

                return true;
            }
            else{
                return false;
            }
        }
        
    // public function listLoaiPhim($perPage = 15, $currentPage = null,$query = null){
    //     $loaiphim = $this->loaiphim->getLoaiPhimQuery();

    //     return $loaiphim->get();
    //     }
          

}
