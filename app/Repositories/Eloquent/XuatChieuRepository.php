<?php
namespace App\Repositories\Eloquent;

use App\Models\Xuatchieu;
use App\Repositories\Contracts\XuatChieuRepositoryInterface;

class XuatChieuRepository implements XuatChieuRepositoryInterface
{
	private $xuatchieu;
	public function __construct() { $this->xuatchieu = new Xuatchieu();}


	public function get($id,$columns = array('*'))
        {
                    $data = $this->xuatchieu->find($id, $columns);
                        if ($data)
                        {
                            return $data;
                        }
                        return null;

        }
	public function all($columns = array('*'))
        {
            $listData = $this->xuatchieu->get($columns);
            return $listData;
        }
	public function paginate($perPage = 15,$columns = array('*'))
        {
            $listData = $this->xuatchieu->paginate($perPage, $columns);
            return $listData;
        }
	public function save(array $data)
        {
        return $this->xuatchieu->create($data);

        }
	public function update(array $data,$id) {
         $dep =  $this->xuatchieu->find($id);
        if ($dep)
        {
            foreach ($dep->getFillable() as $field)
            {
                if (array_key_exists($field,$data)){
                    $dep->$field = $data[$field];
                }
            }
            if ($dep->save())
            {
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
        }
	public function getByColumn($column,$value,$columnsSelected = array('*'))
        {

             $data = $this->xuatchieu->where($column,$value)->first();
            if ($data)
            {
                return $data;
            }
            return null;


        }
	public function getByMultiColumn(array $where,$columnsSelected = array('*'))
        {

             $data = $this->xuatchieu;

            foreach ($where as $key => $value) {
                $data = $data->where($key, $value);
            }

            $data = $data->first();


            if ($data)
            {
                return $data;
            }
            return null;


        }
	public function getListByColumn($column,$value,$columnsSelected = array('*'))
        {

             $data = $this->xuatchieu->where($column,$value)->get();
            if ($data)
            {
                return $data;
            }
            return null;


        }
	public function getListByMultiColumn(array $where,$columnsSelected = array('*'))
        {

             $data = $this->xuatchieu;

              foreach ($where as $key => $value) {
            $data = $data->where($key, $value);
        }

        $data = $data->get();

            if ($data)
            {
                return $data;
            }
            return null;


        }
	public function delete($id)
        {
            $del = $this->xuatchieu->find($id);
            if ($del !== null)
            {
                $del->delete();
                return true;
            }
            else{
                return false;
            }
        }

	public function deleteMulti(array $data)
        {
            $del = $this->xuatchieu->whereIn("id",$data["list_id"])->delete();
            if ($del)
            {

                return true;
            }
            else{
                return false;
            }
        }
        
    public function listXuatChieu($perPage = 15, $currentPage = null,$query = null){
        $xuatchieu = $this->xuatchieu->getXuatChieuQuery()->get();
        //dd($xuatchieu);
        return $xuatchieu;
        }
          

}
