<?php
namespace App\Repositories\Eloquent;

use App\Models\News;
use App\Repositories\Contracts\NewsRepositoryInterface;

class NewsRepository implements NewsRepositoryInterface
{
	private $new;
	public function __construct() { $this->new = new News();}


	public function get($id,$columns = array('*'))
        {
                    $data = $this->new->find($id, $columns);
                        if ($data)
                        {
                            return $data;
                        }
                        return null;

        }
	public function all($columns = array('*'))
        {
            $listData = $this->new->get($columns);
            return $listData;
        }
	public function paginate($perPage = 15,$columns = array('*'))
        {
            $listData = $this->new->paginate($perPage, $columns);
            return $listData;
        }
	public function save(array $data)
        {
        return $this->new->create($data);

        }
	public function update(array $data,$id) {
         $dep =  $this->new->find($id);
        if ($dep)
        {
            foreach ($dep->getFillable() as $field)
            {
                if (array_key_exists($field,$data)){
                    $dep->$field = $data[$field];
                }
            }
            if ($dep->save())
            {
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
        }
	public function getByColumn($column,$value,$columnsSelected = array('*'))
        {

             $data = $this->new->where($column,$value)->first();
            if ($data)
            {
                return $data;
            }
            return null;


        }
	public function getByMultiColumn(array $where,$columnsSelected = array('*'))
        {

             $data = $this->new;

            foreach ($where as $key => $value) {
                $data = $data->where($key, $value);
            }

            $data = $data->first();


            if ($data)
            {
                return $data;
            }
            return null;


        }
	public function getListByColumn($column,$value,$columnsSelected = array('*'))
        {

             $data = $this->new->where($column,$value)->get();
            if ($data)
            {
                return $data;
            }
            return null;


        }
	public function getListByMultiColumn(array $where,$columnsSelected = array('*'))
        {

             $data = $this->new;

              foreach ($where as $key => $value) {
            $data = $data->where($key, $value);
        }

        $data = $data->get();

            if ($data)
            {
                return $data;
            }
            return null;


        }
	public function delete($id)
        {
            $del = $this->new->find($id);
            if ($del !== null)
            {
                $del->delete();
                return true;
            }
            else{
                return false;
            }
        }

	public function deleteMulti(array $data)
        {
            $del = $this->new->whereIn("id",$data["list_id"])->delete();
            if ($del)
            {

                return true;
            }
            else{
                return false;
            }
        }
        
    public function listnew($perPage = 15, $currentPage = null,$query = null){
        $new = $this->new->getNewQuery()->orderBy('id','desc')->paginate(8);

        return $new;
        }
    public function listnew12($perPage = 15, $currentPage = null,$query = null){
        $new = $this->new->getNewQuery();

        return $new->get();
        }    
    public function listnew1($perPage = 15, $currentPage = null,$query = null){
        $new = $this->new->getNewQuery();
        
        $new = $this->new->take(6)->orderBy('id','desc');

        return $new->get();
        }    

    public function UpdateNews(){
        $new = $this->new->getNewQuery();

        $new = $this->new->where('status','=','1')->take(6)->orderBy('id','desc');

        return $new->get();
    }

     public function HotNews(){
        $new = $this->new->getNewQuery();

        $new = $this->new->where('status','=','2');

        return $new->get();
    }      

}
