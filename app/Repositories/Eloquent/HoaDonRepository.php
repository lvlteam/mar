<?php
namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\HoaDonRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Models\bill;
use App\Models\Custommer;
use File;
use Illuminate\Support\Facades\DB;

class HoaDonRepository implements HoaDonRepositoryInterface
{
    private  $hd;
    private  $kh;
    public function __construct() { $this->hd = new bill();
        $this->kh = new Custommer();}


    public function get($id,$columns = array('*'))
    {
        $data = $this->hd->find($id, $columns);
        if ($data)
        {
            return $data;
        }
        return null;

    }
    public function all($columns = array('*'))
    {
        $listData = $this->hd->get($columns);
        return $listData;
    }
    public function paginate($perPage = 15,$columns = array('*'))
    {
        $listData = $this->hd->paginate($perPage, $columns);
        return $listData;
    }
    public function save(array $data)
    {
        return $this->hd->create($data);

    }
    public function update(array $data,$id) {
        $dep =  $this->hd->find($id);
        if ($dep)
        {
            foreach ($dep->getFillable() as $field)
            {
                if (array_key_exists($field,$data)){
                    $dep->$field = $data[$field];
                }
            }
            if ($dep->save())
            {
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
    }
    public function getByColumn($column,$value,$columnsSelected = array('*'))
    {

        $data = $this->hd->where($column,$value)->first();
        if ($data)
        {
            return $data;
        }
        return null;


    }
    public function getByMultiColumn(array $where,$columnsSelected = array('*'))
    {

        $data = $this->hd;

        foreach ($where as $key => $value) {
            $data = $data->where($key, $value);
        }

        $data = $data->first();


        if ($data)
        {
            return $data;
        }
        return null;


    }
    public function getListByColumn($column,$value,$columnsSelected = array('*'))
    {

        $data = $this->hd->where($column,$value)->get();
        if ($data)
        {
            return $data;
        }
        return null;


    }
    public function getListByMultiColumn(array $where,$columnsSelected = array('*'))
    {

        $data = $this->hd;

        foreach ($where as $key => $value) {
            $data = $data->where($key, $value);
        }

        $data = $data->get();

        if ($data)
        {
            return $data;
        }
        return null;


    }
    public function delete($id)
    {
        $del = $this->hd->find($id);
        if ($del !== null)
        {
            $del->delete();
            return true;
        }
        else{
            return false;
        }
    }

    public function deleteMulti(array $data)
    {
        $del = $this->hd->whereIn("id",$data["list_id"])->delete();
        if ($del)
        {

            return true;
        }
        else{
            return false;
        }
    }
    public function getListHoaDon($perPage = 15, $currentPage = null, $query = null)
    {
        $searchValue = null;
        $searchOption = null;
        $orderBy = null;
        $orderBySTT = null;
        $loai_sp = null;
        $unit = null;
        $khuyen_mai =null;

        $list = null;
        $list = $this->hd;
        if ($query != null) {
            if (isset($query['searchValue']) && $query['searchValue'] != '' && $query['searchOption'] == 2) {
                $list = $list->Where('name','like','%'.$query['searchValue'].'%');
            }
            if (isset($query['searchValue']) && $query['searchValue'] != '' && $query['searchOption'] == 3) {
                $list = $list->Where('unit_price','like','%'.$query['searchValue'].'%');
            }
            if(isset($query['orderBy']) && $query['orderBy'] == 1 && $query['orderBySTT'] == 1){
                $list = $list->orderBy('name','asc');
            }
            if(isset($query['orderBy']) && $query['orderBy'] == 1 && $query['orderBySTT'] == 2){
                $list = $list->orderBy('name','desc');
            }
            if(isset($query['orderBy']) && $query['orderBy'] == 2 && $query['orderBySTT'] == 1){
                $list = $list->orderBy('unit_price','asc');
            }
            if(isset($query['orderBy']) && $query['orderBy'] == 2 && $query['orderBySTT'] == 2){
                $list = $list->orderBy('unit_price','desc');
            }
            if(isset($query['orderBy']) && $query['orderBy'] == 1 && $query['orderBySTT'] == 1){
                $list = $list->orderBy('promotion_price','asc');
            }
            if(isset($query['orderBy']) && $query['orderBy'] == 1 && $query['orderBySTT'] == 2){
                $list = $list->orderBy('promotion_price','desc');
            }
            if (isset($query['loai_sp']) && $query['loai_sp'] != '') {
                $loai_sp = $query['loai_sp'];
            }
            if($loai_sp != null){
                $list = $list->where('id_type',$loai_sp);
            }
            if (isset($query['paginate'])){
                $list = $list->paginate($query['paginate']);
            }
            else{
                $list = $list->paginate(5);
            }
            if ($query != null){
                $list->appends($query);
            }
        }
        return [
            'data' => $list,
            'paginate' => $list->links(),
            'currentPage' => $list->currentPage(),
            'perPage' => $list->perPage(),
            'total' => $list->total()
        ];

    }
//    public function khuyen_mai($query = null){
//        $khuyen_mai =null;
//        $list = null;
//        $list = $this->sp;
//        if($query != null){
//            if (isset($query['khuyen_mai']) && $query['khuyen_mai'] != '') {
//                $khuyen_mai = $query['khuyen_mai'];
//            }
//            if($khuyen_mai != null){
//                $list = $list->where('promotion_price','>',0,$khuyen_mai);
//            }
//
//        }
//        return [
//            'data' => $list
//        ];
//        //dd($list);
//
//    }

}
