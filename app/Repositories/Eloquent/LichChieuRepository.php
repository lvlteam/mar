<?php
namespace App\Repositories\Eloquent;

use App\Models\Lichchieu;
use App\Repositories\Contracts\LichChieuRepositoryInterface;
use App\Repositories\Contracts\PhongRepositoryInterface;
use App\Models\Phong;


class LichChieuRepository implements LichChieuRepositoryInterface
{
	private $lichchieu;
    private $phong;
	public function __construct() { $this->lichchieu = new Lichchieu();
                                    $this->phong = new Phong();}


	public function get($id,$columns = array('*'))
        {
                    $data = $this->lichchieu->find($id, $columns);
                        if ($data)
                        {
                            return $data;
                        }
                        return null;

        }
	public function all($columns = array('*'))
        {
            $listData = $this->lichchieu->get($columns);
            return $listData;
        }
	public function paginate($perPage = 15,$columns = array('*'))
        {
            $listData = $this->lichchieu->paginate($perPage, $columns);
            return $listData;
        }
	public function save(array $data)
        {
        return $this->lichchieu->create($data);

        }
	public function update(array $data,$id) {
         $dep =  $this->lichchieu->find($id);
        if ($dep)
        {
            foreach ($dep->getFillable() as $field)
            {
                if (array_key_exists($field,$data)){
                    $dep->$field = $data[$field];
                }
            }
            if ($dep->save())
            {
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
        }
	public function getByColumn($column,$value,$columnsSelected = array('*'))
        {

             $data = $this->lichchieu->where($column,$value)->first();
            if ($data)
            {
                return $data;
            }
            return null;


        }
	public function getByMultiColumn(array $where,$columnsSelected = array('*'))
        {

             $data = $this->lichchieu;

            foreach ($where as $key => $value) {
                $data = $data->where($key, $value);
            }

            $data = $data->first();


            if ($data)
            {
                return $data;
            }
            return null;


        }
	public function getListByColumn($column,$value,$columnsSelected = array('*'))
        {

             $data = $this->lichchieu->getLichChieuQuery()->where($column,$value)->get();
            if ($data)
            {
                return $data;
            }
            return null;


        }
	public function getListByMultiColumn(array $where,$columnsSelected = array('*'))
        {

             $data = $this->lichchieu->getLichChieuQuery();

              foreach ($where as $key => $value) {
            $data = $data->where($key, $value);
        }

        $data = $data->get();

            if ($data)
            {
                return $data;
            }
            return null;


        }
	public function delete($id)
        {
            $del = $this->lichchieu->find($id);
            if ($del !== null)
            {
                $del->delete();
                return true;
            }
            else{
                return false;
            }
        }

	public function deleteMulti(array $data)
        {
            $del = $this->lichchieu->whereIn("id",$data["list_id"])->delete();
            if ($del)
            {

                return true;
            }
            else{
                return false;
            }
        }
        
        public function listChieu($query){

           $lichchieu = $this->lichchieu->getLichChieuQuery();

           $lichchieu = $lichchieu->whereIn('id_phong', [1,2,3,4,5,6,7,8]);

           $lichchieu = $this->fillterLichChieu($lichchieu,$query)->get();

           //dd($lichchieu);
           return $lichchieu;


        }

        public function fillterLichChieu($list,$query){
            if ($query != null) {
                if (isset($query['id_rap']) && $query['id_rap'] != '') {

                    $list = $list->whereHas('rap', function ($list) use ($query) {

                        $list->where('id', $query['id_rap']);
                    });
                }
                if (isset($query['ngaychieu']) && $query['ngaychieu'] != '') {

                    $list->where('ngaychieu', $query['ngaychieu']);
                }
                if (isset($query['id_phim']) && $query['id_phim'] != '') {

                    $list = $list->whereHas('phim', function ($list) use ($query) {

                        $list->where('tenphim', $query['id_phim']);
                    });
                }
            }
            return $list;
        }

        

       
          

}
