<?php
namespace App\Repositories\Eloquent;

use App\Models\Quocgia;
use App\Repositories\Contracts\QuocGiaRepositoryInterface;

class QuocGiaRepository implements QuocGiaRepositoryInterface
{
	private $quocgia;
	public function __construct() { $this->quocgia = new Quocgia();}


	public function get($id,$columns = array('*'))
        {
                    $data = $this->quocgia->find($id, $columns);
                        if ($data)
                        {
                            return $data;
                        }
                        return null;

        }
	public function all($columns = array('*'))
        {
            $listData = $this->quocgia->get($columns);
            return $listData;
        }
	public function paginate($perPage = 15,$columns = array('*'))
        {
            $listData = $this->quocgia->paginate($perPage, $columns);
            return $listData;
        }
	public function save(array $data)
        {
        return $this->quocgia->create($data);

        }
	public function update(array $data,$id) {
         $dep =  $this->quocgia->find($id);
        if ($dep)
        {
            foreach ($dep->getFillable() as $field)
            {
                if (array_key_exists($field,$data)){
                    $dep->$field = $data[$field];
                }
            }
            if ($dep->save())
            {
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
        }
	public function getByColumn($column,$value,$columnsSelected = array('*'))
        {

             $data = $this->quocgia->where($column,$value)->first();
            if ($data)
            {
                return $data;
            }
            return null;


        }
	public function getByMultiColumn(array $where,$columnsSelected = array('*'))
        {

             $data = $this->quocgia;

            foreach ($where as $key => $value) {
                $data = $data->where($key, $value);
            }

            $data = $data->first();


            if ($data)
            {
                return $data;
            }
            return null;


        }
	public function getListByColumn($column,$value,$columnsSelected = array('*'))
        {

             $data = $this->quocgia->where($column,$value)->get();
            if ($data)
            {
                return $data;
            }
            return null;


        }
	public function getListByMultiColumn(array $where,$columnsSelected = array('*'))
        {

             $data = $this->quocgia;

              foreach ($where as $key => $value) {
            $data = $data->where($key, $value);
        }

        $data = $data->get();

            if ($data)
            {
                return $data;
            }
            return null;


        }
	public function delete($id)
        {
            $del = $this->quocgia->find($id);
            if ($del !== null)
            {
                $del->delete();
                return true;
            }
            else{
                return false;
            }
        }

	public function deleteMulti(array $data)
        {
            $del = $this->quocgia->whereIn("id",$data["list_id"])->delete();
            if ($del)
            {

                return true;
            }
            else{
                return false;
            }
        }
        
    public function listQuocGia($perPage = 15, $currentPage = null,$query = null){
        $Quocgia = $this->quocgia->getQuocGiaQuery();

        return $Quocgia->get();
        }
          

}
