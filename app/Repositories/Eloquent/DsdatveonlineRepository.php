<?php
namespace App\Repositories\Eloquent;

use App\Models\Dsdatveonline;
use App\Repositories\Contracts\DsdatveonlineRepositoryInterface;

class DsdatveonlineRepository implements DsdatveonlineRepositoryInterface
{
	private $dsdatveonline;
	public function __construct() { $this->dsdatveonline = new Dsdatveonline();}


	public function get($id,$columns = array('*'))
        {
                    $data = $this->dsdatveonline->find($id, $columns);
                        if ($data)
                        {
                            return $data;
                        }
                        return null;

        }
	public function all($columns = array('*'))
        {
            $listData = $this->dsdatveonline->get($columns);
            return $listData;
        }
	public function paginate($perPage = 15,$columns = array('*'))
        {
            $listData = $this->dsdatveonline->paginate($perPage, $columns);
            return $listData;
        }
	public function save(array $data)
        {
        return $this->dsdatveonline->create($data);

        }
	public function update(array $data,$id) {
         $dep =  $this->dsdatveonline->find($id);
        if ($dep)
        {
            foreach ($dep->getFillable() as $field)
            {
                if (array_key_exists($field,$data)){
                    $dep->$field = $data[$field];
                }
            }
            if ($dep->save())
            {
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
        }
	public function getByColumn($column,$value,$columnsSelected = array('*'))
        {

             $data = $this->dsdatveonline->where($column,$value)->first();
            if ($data)
            {
                return $data;
            }
            return null;


        }
	public function getByMultiColumn(array $where,$columnsSelected = array('*'))
        {

             $data = $this->dsdatveonline;

            foreach ($where as $key => $value) {
                $data = $data->where($key, $value);
            }

            $data = $data->first();


            if ($data)
            {
                return $data;
            }
            return null;


        }
	public function getListByColumn($column,$value,$columnsSelected = array('*'))
        {

             $data = $this->dsdatveonline->where($column,$value)->get();
            if ($data)
            {
                return $data;
            }
            return null;


        }
	public function getListByMultiColumn(array $where,$columnsSelected = array('*'))
        {

             $data = $this->dsdatveonline;

              foreach ($where as $key => $value) {
            $data = $data->where($key, $value);
        }

        $data = $data->get();

            if ($data)
            {
                return $data;
            }
            return null;


        }
	public function delete($id)
        {
            $del = $this->dsdatveonline->find($id);
            if ($del !== null)
            {
                $del->delete();
                return true;
            }
            else{
                return false;
            }
        }

	public function deleteMulti(array $data)
        {
            $del = $this->dsdatveonline->whereIn("id",$data["list_id"])->delete();
            if ($del)
            {

                return true;
            }
            else{
                return false;
            }
        }
        
    public function listDanhSachDatVe($perPage = 15, $currentPage = null,$query = null){
        $dsdatveonline = $this->dsdatveonline->getDsdatveonlineQuery();
        
        return $dsdatveonline->get();
        }  

}
