<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

abstract class UserRE {
    const DSDATVEONLINE = 'dsdatveonline';
    const GROUP = 'group';

}

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','id_group','phone','status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function group(){
        return $this->belongsTo('App\UserGroup','id_group');
    }

    public function dsdatveonline(){
        return $this->hasMany('App\Models\Dsdatveonline','id_group');
    }

    public function getUserQuery()
    {
        return $this->with(UserRE::GROUP)
                    ->with(UserRE::DSDATVEONLINE);
    }
}
