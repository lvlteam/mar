<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->to('/admin');

});

Auth::routes();
Route::group(['middleware'=>'admin'],function (){
    Route::get('/admin/excel',function (){
        \Excel::create('Filename', function($excel) {
            $excel->sheet('TongHopSoLieuDanCu');
            $excel->setTitle('Our new awesome title');
        })->store('xls',storage_path('excel/exports'))->export('xls');
    });
    //xét route cho trang quản trị
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/admin', 'Admin\DashBoardController@index')->name('admin');
    Route::get('/error', 'Admin\DefaultController@error')->name('default.error');


    Route::get('/admin/user/add', 'Admin\UserController@addUser')->name('admin.user.add');
    Route::get('/admin/user/edit/{id}', 'Admin\UserController@editUser')->name('admin.user.edit');
    Route::get('/admin/user/list', 'Admin\UserController@listUser')->name('admin.user.list');
    Route::post('/admin/user/save', 'Admin\UserController@save')->name('admin.user.save');
    Route::post('/admin/user/update/{id}', 'Admin\UserController@update')->name('admin.user.update');

    Route::get('/admin/user/delete/{id}', 'Admin\UserController@deleteUser')->name('admin.user.delete');

    Route::get('/admin/user/profile/{id}', 'Admin\UserController@getProfile')->name('admin.user.get_profile');
    Route::post('/admin/user/save-profile/{id}', 'Admin\UserController@updateProfile')->name('admin.user.save_profile');

    //ql group user
    Route::get('/admin/group/list', 'Admin\UserGroupController@index')->name('admin.group.list');
    Route::get('/admin/group/add', 'Admin\UserGroupController@form')->name('admin.group.add');
    Route::post('/admin/group/add', 'Admin\UserGroupController@add')->name('admin.group.add');

    Route::get('/admin/group/edit/{id}', 'Admin\UserGroupController@form')->name('admin.group.edit');
    Route::post('/admin/group/edit/{id}', 'Admin\UserGroupController@edit')->name('admin.group.edit');

    Route::get('/admin/group/permission/{id}', 'Admin\UserGroupController@permission')->name('admin.group.permission');
    Route::post('/admin/group/permission/{id}', 'Admin\UserGroupController@save_permission')->name('admin.group.permission');

    Route::get('/admin/group/delete/{id}', 'Admin\UserGroupController@delete')->name('admin.group.delete');

    //loaiphim
    Route::get('admin/loai-phim/list','Admin\LoaiPhimController@index')->name('admin.loai-phim.list');
    Route::get('admin/loai-phim/add','Admin\LoaiPhimController@create')->name('admin.loai-phim.create');
    Route::post('admin/loai-phim/save','Admin\LoaiPhimController@store')->name('admin.loai-phim.store');
    Route::get('admin/loai-phim/show/{id}','Admin\LoaiPhimController@show')->name('admin.loai-phim.show');
    Route::get('admin/loai-phim/edit/{id}','Admin\LoaiPhimController@edit')->name('admin.loai-phim.edit');
    Route::post('admin/loai-phim/update/{id}','Admin\LoaiPhimController@update')->name('admin.loai-phim.update');
    Route::get('admin/loai-phim/delete/{id}','Admin\LoaiPhimController@delete')->name('admin.loai-phim.destroy');
});

    // xét route cho trang web 
    Route::get('Home','Camera\HomeController@index')->name('camera.home.list');
    Route::get('promotion','Camera\PromotionController@index')->name('promotion.list');
    Route::get('contact','Camera\ContactController@index')->name('contact.list');
    Route::get('list_of_product','Camera\ListOfProductController@index')->name('list_of_product.list');
    Route::get('product_detail','Camera\ProductDetailController@index')->name('prodcut_detail.list');
    Route::get('cart','Camera\CartController@index')->name('cart.list');
    Route::get('order','Camera\OrderController@index')->name('order.list');
    Route::get('ordersuccess','Camera\OrderSuccessController@index')->name('ordersuccess.list');
    Route::get('sendsuccess','Camera\SendSuccessController@index')->name('send.list');



//google
Route::get('login/google', 'Auth\LoginController@googleRedirectToProvider')->name('google');
Route::get('login/google/callback', 'Auth\LoginController@googleHandleProviderCallback');